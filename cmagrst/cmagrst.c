/* cmagrst.c, Boone, 05/06/06
   MAGRST clone, reads .TAP files */

/* Modifications:
   05/06/06 Boone      Initial coding
   End Modifications */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAXBUF 65536

unsigned int lastseqno;
int endoftape;
int errorrec;
int insegdir;
int debug;
unsigned char pathname[4096];
int pi;
FILE *ofh;
int filtyp;

void deslash(unsigned char path[], int pathlen)
{
	int i;

	for (i = 0; i < pathlen; i++)
		if (path[i] == '/')
			path[i] = '_';
}

void dumpdatarec(unsigned char recbuf[MAXBUF], unsigned short cnt)
{
	int i;
	int j;
	unsigned int c;

	for (i = 0; i < cnt; i += 16)
	{
		for (j = 0; j < 16; j++)
		{
			c = recbuf[i+j];
			if (debug)
				fprintf(stderr, "%02x ", c);
		}
		if (debug)
			fputc('|', stderr);
		for (j = 0; j < 16; j++)
		{
			c = recbuf[i+j];
			c &= 0177;
			if ((c < 040) || (c > 0176))
				c = '.';
			if (debug)
				fputc(c, stderr);
		}
		if (debug)
			fputs("|\n", stderr);
	}
}

unsigned int readrec(unsigned char recbuf[MAXBUF])
{
	int i;
	unsigned int blocksize = 0;
	ssize_t nc;
	ssize_t rnc;

	for (i = 0; i < MAXBUF; i++)
		recbuf[i] = 0;

	nc = read(fileno(stdin), &blocksize, 4);
	if (nc == 0)
	{
		if (debug)
			fputs("physical end-of-file\n", stderr);
		exit(0);
	}
	if (debug)
		fprintf(stderr, "read pre tap: nc %u, tap block size %u\n",
				nc, blocksize);
	if (blocksize == 0)
		return(0);
	if (blocksize == 0xffffffff)
	{
		if (debug)
			fputs("Logical end-of-tape\n", stderr);
		endoftape = 1;
		return(0);
	}
	if (blocksize & 0x80000000)
	{
		errorrec = 1;
		return(0);
	}
	rnc = read(fileno(stdin), recbuf, blocksize);
	if (debug)
		fprintf(stderr, "read data nc %u\n", rnc);
	nc = read(fileno(stdin), &blocksize, 4);
	if (debug)
		fprintf(stderr, "read post tap: nc %u, tap block size %u\n",
				nc, blocksize);
/*	dumpdatarec(recbuf, blocksize); */
	return(rnc);
}

void dumprden$$(unsigned char recbuf[48])
{
	int ecwtype;
	char *ecwtypnam;
	int ecwlen;
	char *filtypnam;
	int year, month, day, time;
	int hour, minute;
	int i;
	unsigned int c;
	int segfiletyp;
	int segfileno;
	unsigned char cmd[4096];

	ecwtype = recbuf[0];
	switch (ecwtype)
	{
		case 0:
			ecwtypnam = "ufdhdr";
			break;
		case 1:
			ecwtypnam = "oldufd";
			break;
		case 2:
			ecwtypnam = "newufd";
			break;
		default:
			ecwtypnam = "unknown";
			break;
	}
	ecwlen = recbuf[1];
	filtyp = recbuf[39];
	switch (filtyp)
	{
		case 0:
			filtypnam = "SAM";
			break;
		case 1:
			filtypnam = "DAM";
			break;
		case 2:
			filtypnam = "SEGSAM";
			break;
		case 3:
			filtypnam = "SEGDAM";
			break;
		case 4:
			filtypnam = "UFD4";
			break;
		case 5:
			filtypnam = "UFD5";
			break;
		case 6:
			filtypnam = "DefCATACL";
			break;
		case 0376:
			filtypnam = "ACL";
			break;
		case 0377:
			filtypnam = "UseCATACL";
			break;
		default:
			filtypnam = "unknown";
			break;
	}
	if (! insegdir)
	{
		year = (recbuf[40] & 0376) >> 1;
		month = ((recbuf[40] & 01) << 3) | ((recbuf[41] >> 5) & 07);
		day = recbuf[41] & 037;
		time = (recbuf[42] << 8) | recbuf[43];
		time *= 4;
		hour = time / 3600;
		time %= 3600;
		minute = time / 60;
		time %= 60;
		if (debug)
			fputs("\t[", stderr);
		if (pi > 0)
		{
			pathname[pi] = '/';
			pi++;
			pathname[pi] = 0;
		}
		for (i = 2; i < 35; i++)
		{
			c = recbuf[i];
			c &= 0177;
			if ((c > 040) && (c < 0177))
			{
				if (c == '/')
					c = '_';
				pathname[pi] = c;
				pathname[pi+1] = 0;
				pi++;
				if (debug)
					fputc(c, stderr);
			}
		}
		if (debug)
			fprintf(stderr, "] ecw %s len %d, filtyp %s "
					"mod %02d-%02d-%02d time %02d:%02d:%02d\n",
					ecwtypnam, ecwlen, filtypnam, year, month, day, hour,
					minute, time);
		if (debug)
			fprintf(stderr, "pathname [%s] pi [%d]\n", pathname, pi);
		puts(pathname);
		switch (filtyp)
		{
			case 0:
				if (ofh != NULL)
					fclose(ofh);
				if ((ofh = fopen(pathname, "w")) == NULL)
					perror(pathname);
				filtypnam = "SAM";
				break;
			case 1:
				if (ofh != NULL)
					fclose(ofh);
				if ((ofh = fopen(pathname, "w")) == NULL)
					perror(pathname);
				filtypnam = "DAM";
				break;
			case 2:
				mkdir(pathname, 0770);
				filtypnam = "SEGSAM";
				break;
			case 3:
				mkdir(pathname, 0770);
				filtypnam = "SEGDAM";
				break;
			case 4:
				mkdir(pathname, 0770);
				filtypnam = "UFD4";
				break;
			case 5:
				mkdir(pathname, 0770);
				filtypnam = "UFD5";
				break;
			case 6:
				filtypnam = "DefCATACL";
				break;
			case 0376:
				filtypnam = "ACL";
				break;
			case 0377:
				filtypnam = "UseCATACL";
				break;
			default:
				filtypnam = "unknown";
				break;
		}
	}
	else
	{
		segfiletyp = (recbuf[0] << 8) | recbuf[1];
		segfileno = (recbuf[4] << 8) | recbuf[5];
		if (debug)
			fprintf(stderr, "\t[%d], seg member type %s\n", segfileno,
				segfiletyp == 0 ? "SAM" : "DAM");
	}
	if ((filtyp == 2) || (filtyp == 3))
		insegdir = 1;
}

void dumptreerec(unsigned char recbuf[MAXBUF], unsigned short cnt)
{
	int i;

	insegdir = 0;
	pi = 0;
	pathname[0] = 0;
	for (i = 6; i < cnt; i += 48)
	{
		dumprden$$(&recbuf[i]);
	}
}

void dumpmsrec(unsigned char recbuf[MAXBUF])
{
	unsigned short seq, cnt, rct;
	int i;

	seq = recbuf[0] << 8 | recbuf[1];
	cnt = recbuf[2] << 8 | recbuf[3];
	rct = recbuf[4] << 8 | recbuf[5];
	cnt *= 2;
	if (debug)
		fprintf(stderr, "sequence %u count %u rectype %d\n", seq, cnt, rct);
	switch(rct)
	{
		case 1:
			if (debug)
				fputs("data record\n", stderr);
			if (filtyp == 0 || filtyp == 1)
				if (fwrite(&recbuf[6], 1, cnt-6, ofh) < (cnt-6))
					perror("data write");
			dumpdatarec(recbuf, cnt);
			break;
		case 2:
			if (debug)
				fputs("treename record\n", stderr);
			dumptreerec(recbuf, cnt);
			dumpdatarec(recbuf, cnt);
			break;
		case 4:
			if (debug)
				fputs("logical tape  record\n", stderr);
			dumpdatarec(recbuf, cnt);
			break;
		case 5:
			if (debug)
				fputs("end of logical tape record\n", stderr);
			dumpdatarec(recbuf, cnt);
			break;
		default:
			if (debug)
				fprintf(stderr, "unknown record type %d\n", rct);
			dumpdatarec(recbuf, cnt);
			break;
	}
}

void readLabel()
{
	unsigned char labelbuf[MAXBUF];
	unsigned int blocksize = 99999;

	if (debug)
		fputs("reading label\n", stderr);
	while (blocksize > 0)
		blocksize = readrec(labelbuf);
}

void readData()
{
	unsigned char datarecbuf[MAXBUF];
	unsigned int seqno;
	unsigned int blocksize = 99999;

	if (debug)
		fputs("reading data records\n", stderr);
	while (! endoftape)
	{
		blocksize = readrec(datarecbuf);
		if (errorrec)
		{
			errorrec = 0;
			if (debug)
				fputs("tape error\n", stderr);
			continue;
		}
		seqno = (datarecbuf[0] << 8) | datarecbuf[1];
		if (debug)
			if (seqno != lastseqno+1)
				fprintf(stderr, ">>> out of sequence records: %d %d\n",
					lastseqno, seqno);
		dumpmsrec(datarecbuf);
		lastseqno = seqno;
	}
}

void skipTM()
{
	unsigned char emptybuf[MAXBUF];
	unsigned int blocksize;

	while (1)
	{
		blocksize = readrec(emptybuf);
		if (errorrec)
			continue;
		break;
	}
}

void skipFile(int skipcount)
{
	int i;
	unsigned int blocksize;
	unsigned int nc;
	unsigned char scratchbuf[MAXBUF];

	for (i = 0; i < skipcount; i++)
	{
		if (debug)
			fprintf(stderr, "skipping a boot file\n");
		nc = 1;
		while (nc != 0)
		{
			nc = readrec(scratchbuf);
			if (errorrec)
			{
				errorrec = 0;
				nc = 1;
			}
			if (debug)
				fprintf(stderr, "	skipping %u byte record\n", nc);
		}
	}
}

int main(int argc, char *argv[])
{
	int skipboot;

	lastseqno = 0;
	endoftape = 0;
	errorrec = 0;
	ofh = NULL;
	debug = 1;
	skipboot = 0;

	while (1)
	{
		readLabel();
		skipFile(skipboot);
		readData();
		skipTM();
	}
}
