/*
 *  Copy.c
 *  Copyright (c) 1990 Stephen T. Hinchey
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#define DELETE  (flags & 1)                       /* 000000001               
*/
#define FORCE   (flags & 2)                       /* 000000010               
*/
#define REPORT  (flags & 4)                       /* 000000100               
*/
#define NEWMODE (flags & 8)                       /* 000001000               
*/
#define CHOWN   (flags & 16)                      /* 000010000               
*/
#define DIR     (flags & 32)                      /* 000100000               
*/

main (argc, argv)
int   argc;
char *argv[];
{
   char sourceFile[132],                          /* File being copied       
*/
        targetFile[132],                          /* New file                
*/
        targetDir[132],                           /* New directory           
*/
        Ustring[140],                             /* Uppercase string        
*/
        buff[BUFSIZ];                             /* I/O buffer              
*/

   struct stat statptr, sptr;

   int flags=0,                                   /* Flags for options       
*/
       indx,                                      /* Indexing variable       
*/
       slimit,
       tlimit,
       xlimit,
       byteLimit,
       result,                                    /* Result of function      
*/
       newMode=0;
   int unlink ();                                 /* Deletes file            
*/

   FILE *ifp, *ofp,                               /* File pointers           
*/
        *openfile();

   void help(),
        strUpper(),
        parseFile();

   if (argc < 2)
   {                                              /* Not enough arguments    
*/
      help ();                                    /* Give the user some help 
*/
      return (0);                                 /* No error on return      
*/
   }

   sourceFile[0] = '\0';                          /* Init to null string     
*/

   for (indx = 1; indx < argc; indx++)
   {                                              /* Parse command line      
*/
      strcpy (Ustring, argv[indx]);               /* Make copy of option     
*/
      strUpper (Ustring);                         /* Convert to Uppercase    
*/
      if (Ustring[0] == '-')
      {                                           /* Begin with a dash?      
*/
         if ((strcmp (Ustring, "-H"   ) == 0) ||  /* Asking for help ?       
*/
             (strcmp (Ustring, "-HELP") == 0))
         {
            help ();                              /* Dislay help             
*/
            return (0);                           /* No error                
*/
         }
         else if ((strcmp (Ustring, "-DEL"   ) == 0) ||
                  (strcmp (Ustring, "-DELETE") == 0))
         {
            flags = flags | 1;
         }
         else if ((strcmp (Ustring, "-FRC"  ) == 0) ||
                  (strcmp (Ustring, "-FORCE") == 0))
         {
            flags = flags | 2;
         }
         else if ((strcmp (Ustring, "-RPT"   ) == 0) ||
                  (strcmp (Ustring, "-REPORT") == 0))
         {
            flags = flags | 4;
         }
         else if ((strcmp (Ustring, "-MODE") == 0) ||
                  (strcmp (Ustring, "-MODE") == 0)) 
         {
            flags = flags | 8;
            result = 0;
            indx += 1;
            while (argv[indx][result] != '\0')
            {
               newMode = (newMode * 8) + (argv[indx][result++] - '0');
            }
            argv[indx][0] = '-';
         }
         else if ((strcmp (Ustring, "-OWN"  ) == 0) ||
                  (strcmp (Ustring, "-CHOWN") == 0))
         {
            flags = flags | 16;
         }
         else
         {
            printf ("Unknow option %s\n", Ustring);
            help ();
            return (-1);
         }
      }
      else
      {
         if (strlen (sourceFile) == 0)
         {                                        /* Gotten filename yet? */
            strcpy (sourceFile, argv[indx]);
            parseFile (argv[indx], targetFile);   /* Parse filename */
         }
         else                                     /* Already have source name 
*/
            strcpy (targetFile, argv[indx]);
      }
   }

   result = stat (targetFile, &statptr);          /* Get status of file  */
   tlimit = strlen (targetFile);
   strcpy (targetDir, targetFile);                /* Save target directory */
   targetDir[tlimit + 1] = '\0';                  /* Put terminator in place 
*/
   targetDir[tlimit] = '/';                       /* Put path terminator in */
   if ((statptr.st_mode & S_IFMT) == S_IFDIR)     /* Dir not file?       */
      flags = flags | 32;

   for (indx = 1; indx < argc; indx++)
   {
      if (argv[indx][0] == '-')
         continue;
      slimit = strlen(argv[indx]);
      xlimit = (slimit > tlimit ? slimit : tlimit);
      if (strncmp (argv[indx], targetDir, xlimit) == 0)
         continue;
      if (DIR)
      {
         strcpy (targetFile, targetDir);          /* Copy target dir name    
*/
         parseFile (argv[indx], Ustring);         /* Parse filename          
*/
         strcat (targetFile, Ustring);            /* Append target file name 
*/
      }
      strcpy (sourceFile, argv[indx]);
      result = stat (sourceFile, &sptr);          /* Get status of file  */
      ofp = openfile (targetFile, flags);
      if (ofp != NULL)                            /* File opened OK         */
      {
         if ((ifp = fopen (sourceFile, "rb")) == NULL)
         {
            printf ("Unable to open source file \"%s\"\n", sourceFile);
            fclose (ofp);
            result = unlink (targetFile);         /* Don't leave lying around 
*/
         }

         if (ifp != NULL)                         /* Source file open OK    */
         {
            buff[0] = fgetc(ifp);
            if (ferror(ifp) != 0)
               clearerr(ifp);
            byteLimit = 1;
            while (feof(ifp) == 0) 
            {
               fputc(buff[0], ofp);
               if (ferror(ofp) != 0)
                  clearerr(ofp);
               buff[0] = fgetc(ifp);
               if (ferror(ifp) != 0)
                  clearerr(ifp);
               byteLimit += 1;
            }
            fclose (ofp);
            fclose (ifp);
            if (!NEWMODE)
            {
               newMode  = (sptr.st_mode & S_IREAD);
               newMode += (sptr.st_mode & S_IWRITE);
               newMode += (sptr.st_mode & S_IEXEC);
               newMode += (sptr.st_mode & (S_IREAD >> 3));
               newMode += (sptr.st_mode & (S_IWRITE >> 3));
               newMode += (sptr.st_mode & (S_IEXEC >> 3));
               newMode +=  sptr.st_mode & (S_IREAD >> 6);
               newMode +=  sptr.st_mode & (S_IWRITE >> 6);
               newMode +=  sptr.st_mode & (S_IEXEC >> 6);
            }

            result = chmod(targetFile, newMode);

            if (DELETE)
            {
               result = unlink (sourceFile);
            }
            if (REPORT)
            {
               printf ("%s copied to %s", sourceFile, targetFile);
               if (DELETE && (result == 0))
                  printf (" and deleted");
               if (DELETE && (result != 0))
                  printf (" and NOT \07deleted");
               printf (".\n");
            }
         }
      }
   }
   return(0);
}

/******************************************************************************
/
void parseFile (str, file)
char *str,
     *file;
{
   char *ptr;
   char *strrchr ();

   ptr = strrchr (str, '/');                      /* Find last '/'            
*/
   if (ptr != 0)                                  /* Found a '/'              
*/
   {
      ptr += 1;                                   /* Position beyond '/'      
*/
      strcpy (file, ptr);                         /* Copy filename            
*/
   }
   else                                           /* No '/' found             
*/
      strcpy (file, str);                         /* Copy entire string       
*/
}

/******************************************************************************
/
FILE * openfile (filename, flags)
char *filename;
int   flags;
{
   FILE * ofp;
   char  yesno,
        dummy;
   int result;

   yesno = 'y';
   if (((ofp = fopen (filename, "r")) != NULL) && (!FORCE))
   {
      fclose (ofp);
      printf ("%s already exists, OK to overwrite? (y/n) ", filename);
      yesno = getchar ();
      if (yesno == 'y' || yesno == 'Y')
      {
         result = unlink (filename);
         yesno = 'y';
      }
      else
         ofp = NULL;
      dummy = getchar ();                         /* Get trailing <CR>       
*/
   }
   if (yesno == 'y')
      ofp = fopen (filename, "wb");
   return (ofp);
}
/******************************************************************************
/

void help ()
{
   printf ("\nCopy:\n\n");
   printf ("Copy sourcefile targetfile {-options}\n\n");
   printf ("Sourcefile is the file you are copying.\n");
   printf ("Targetfile is the new file you are creating\n\n");
   printf ("If the sourcefile contains any metacharacters ");
   printf ("or is a list of files\n");
   printf ("to be copied then targetfile must be a directory.\n\n");
   printf ("Options:\n");
   printf ("    -Report (-RPT) report the copy result\n");
   printf ("    -Force  (-FRC) force overwriting of the targetfile\n");
   printf ("    -Delete (-DEL) delete the sourcefile after the copy ");
   printf ("is complete\n");
   printf ("    -Mode filemode  sets the mode for the target file\n");
   printf ("          filemode is an octal number generated by ORing the ");
   printf ("following:\n\n");
   printf ("          0400  Allow Read by owner\n");
   printf ("          0200  Allow Write by owner\n");
   printf ("          0100  Allow Execution by owner\n");
   printf ("          0070  Allow Read, Write & Execution by group\n");
   printf ("          0007  Allow Read, Write & Execution by world\n");
   printf ("\n");
}
