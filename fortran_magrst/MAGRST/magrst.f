C   MAGRSTi.F, WORKER/TAPUTILS, DHQ, 04/10/2002.
C   Reads PRIME MAGSAV files from mag tape. 
C
C   April   2002  Quebbeman       Modified to read TAP tape container image files.
C   July    1994	Hugh Gall       Updated to read Rev 21 and later tapes.
C   October 1989	A.Hulme					Initial coding.
C
C   MAGSAV volumes consist of 3 tape files. The first consists of a single
C   record which is the tape header, the second contains the actual files,
C   and the third is empty.
C
C   Each record in the 2nd starts with a 3 word (6 byte) prefix. The first
C   word is the record number , the second is the length of the record in words
C   (including the prefix, max 2048), and the third is the record type -
C   2=file info, 1=file data, 5=end.
C
C   An info record is a mutiple of 24 words long (excluding the prefix), and
C   describes the relative disk path necessary to reach the file. Each 24 words
C   is copy of the file's or directory's UFD entry, and therefore starts with
C   an ECW entry which is normally hex'218'.
C
C   Command line arguments: r(raw)   - read exactly, do not convert Prime ASCII.
C                           i(index) - display index only.
C                           t        - same as "i" (compatibility with tar).
C
C   Without the "r" option, SEG files and directory information are ignored,
C   except for "partial" restores.
C
C   The '/' character in filenames is converted (silently) to ':' .
C
C   This program does not re-create directory trees. However, the following
C   bourne shell script will do the job from the restored files, which
C   are in the form:  %ddd%ddd%...%fff   ( ddd = directory, fff = file name)

C------------------------------------------------------------------
C   #! /bin/sh
C   if [ $# -lt 1 ]; then  ls | grep "^%" | xargs -i $0 {}
C   else
C     d=`pwd`
C     a=`expr ${1}% : '%\([^%]*\)%.*'`
C     b=`expr $1 : '%[^%]*%\(.*\)'`
C     while [ $b ]; do
C       if [ -d $a ]; then cd $a
C       elif [ -f $a ]; then exit 0
C       else mkdir $a; cd $a
C       fi
C       a=`expr ${b}% : '\([^%]*\)%.*'`
C       b=`expr $b : '[^%]*%\(.*\)'`
C     done
C     if [ -f $a ]; then exit 0; fi
C     mv ${d}/$1 $a ; echo $1
C   fi
C------------------------------------------------------------------
C  Alternative script (works with bash):
C------------------------------------------------------------------
C    #! /bin/sh
C    # mk%dir re-creates directory structure for magrst files.
C    
C    if [ $# -lt 1 ]; then for i in `ls %*` ;do $0 $i; done
C    else
C      d=`pwd`
C      a=`expr ${1}% : '%\\([^%]*\\)%.*'`
C      b=`expr $1 : '%[^%]*%\\(.*\\)'`
C      while [ $b ]; do
C        if [ -d $a ]; then cd $a
C        elif [ -f $a ]; then exit 0
C        else mkdir $a; cd $a
C        fi
C        a=`expr ${b}% : '\\([^%]*\\)%.*'`
C        b=`expr $b : '[^%]*%\\(.*\\)'`
C      done
C      if [ -f $a ]; then exit 0; fi
C      mv ${d}/$1 $a ; echo $1
C    fi
C------------------------------------------------------------------

C   TAP tape container image file logical unit 
      integer tlu
	    
C   Tape volume header
      character*5120 header
      character*1 header1(5120)
      equivalence(header,header1)
      character*8 date
      
C   Data buffer or record
      character*4096 buf
      character*1 buf1(4096)
      equivalence(buf,buf1)
      common buf

C   Tape File number,   record number, record length, record type
      integer    fn ,             rn ,           rl , rtype

C   File type, UFD index
      integer ftype, ufdi
      
C   "segdir" is true if the file is a seg file or a "directory"
	    logical segdir

C   Max treename length
      parameter (nl = 256)
      character*256 name,treenm(100)
      
C   Actual length
	    integer tnml(100)

      character*1 c
      character*16 ans
      character*4 arg

C   Program mode
      logical partial,indx,raw
      
C   "wildcard" is true if a wildcard is input in partial mode. In which
C   case the whole logical tape has to be scanned.
	    logical wildcard

	    logical eol,blnk
	    integer oct221,oct212,mask7

      integer*2 n2
      character*2 n2c
      equivalence(n2,n2c)

      integer*4 n4
      character*4 n4c
      equivalence(n4,n4c)
      
      integer*4 filepos, frontlen, backlen
      integer*2 frontlen1, frontlen2, backlen1, backlen2

	    data oct221/-111/, oct212/-118/, mask7/127/
	    data fn/0/

      partial = .false.
	    indx = .false.
	    raw = .false.
	    wildcard = .false.

C   Get command line argument(s)
	    do 5 i=1,iargc()
	      call getarg(i,arg)
 	      call lcase(arg,4)
	      if((index(arg,'i')+index(arg,'t')).ne.0) indx=.true.
5	    if(index(arg,'r').ne.0) raw=.true.

C   Assign FORTRAN I/O channel to image file
      tlu = 22


	    if(indx) goto 8
6     write(*,"('Enter logical tape number: ',$)")
      read(*,*,err=6) n
	    if(n.lt.0) goto 6
	    if(n.ne.0) goto 8
	    goto 16

C   Open TAP tape image container file
8     continue 

      open (UNIT=tlu, file='INPUT', access='DIRECT', status='OLD',
     &         form='UNFORMATTED', RECL=1)
   
C   Read volume header. This is 5108 bytes long for enhanced MAGSAV (REV21+)
C   4336 bytes long for a rev20 MAGSAV and
C   1024 bytes for a rev19 (and previous).

      ival=1
      
      read(tlu,rec=1) frontlen, header(1:frontlen), backlen
      print *, 'Frontlen = ', frontlen,backlen

12    continue

      write(*,13) (jj,header(jj:jj),jj=1,14)
13    format(' After read header', (/'header(',i2,')= ',o4))
     
10    continue
  
C   Display header information. First convert date to UK format - dd-mm-yy
      date = header(11:12)//'-'//header(9:10)//'-'//header(13:14)
      call rmbit8(date, 8)
      
C   and get version number in n2c (n2)
      n2c = header(15:16)
      call rmbit8(n2c, 2)
      
      call rmbit8(header(19:19), 6)
      write(*,901) header(19:24),date,n2
901   format('Tape header:-   Name:',a6,'    Date:',a8,
     &       '    Version:',a2,/)

C   Skip end-of-file mark after header.
      filepos = (frontlen + 1) + 8
      read(tlu,rec=filepos) backlen
      filepos = filepos + 4
      if(backlen.eq.0) goto 14
      print *, 'TAP EOF mark not read properly...', backlen
      stop

14    continue
      ierr = 0
      if(ierr.ne.0) goto 10

      if(indx) goto 40

C   Prompt for user command. Read reply and convert to lower case
16    write(*,"('Ready to restore: ',$)")
      read(*,'(a)') ans
      call lcase(ans,16)

      if(ans(1:1).eq.'n' .and. ans(2:2).ne.'w') goto 100
      if(ans(1:1).eq.'q') goto 100
      if(ans(1:1).eq.'y') goto 21
      if(ans(1:1).eq.'i' .or. ans(1:2).eq.'nw') goto 22
      if(ans(1:1).eq.'p') goto 24
      if(ans(1:1).eq.'r') goto 23

	    if(raw) then
             print *,'Type: "y" to restore everything,'
	    else
             print *,'Type: "y" to restore all SAM and DAM files'
     .              ,'(with ASCII conversion),'
             print *,'      "r" to restore everything in raw mode'
     .              ,'(no ASCII conversion),'
	    endif
      print *,'      "p" to restore selected files (partial restore),'
      print *,'  or  "i" for just an index.'
      print *
      goto 16

21    continue
      goto 40
22    indx = .true.
      goto 40
23    raw = .true.
	    goto 40
24    partial = .true.

C   Partial restore. Get PRIME treenames
      write(*,"('Type file or directory names one per line exactly',
     .          ' as they appear in the index',
     . /,'(including the % signs),  and a blank line to finish.',
     . /,'A  %  at the end of a line will act as a wildcard.',/)")
      n = 0
30    write(*,"('Treename: ',$)")
      read(*,"(a)") name
      l = lnblnk(name)
      if(l.le.0) goto 34
      n = n + 1
      call lcase(name,l)
C   Convert ">" symbols to "%" and add "%" to front
      i = 0
      j = 0
      if(name(1:1).ne.'%') then
        treenm(n)(1:1) = '%'
        j = 1
      endif
32    i = i + 1
      c = name(i:i)
      if(ichar(c).lt.23) goto 32
      if(c.eq.'>') c = '%'
      j = j + 1
      treenm(n)(j:j) = c
      if(i.lt.l) goto 32
      tnml(n) = j
      if(c.eq.'%') wildcard = .true.
      if(n.lt.100) goto 30

34    if(n.le.0) goto 100

C   Initialise file number and actual record number.
40    fn = 0
      rn = 0

C   Read next UFD entry into buf. Return rec length in rl, type in rtype
42    call rdrec(tlu,filepos,rn,rl,rtype)
43    if(rl.eq.0) goto 100
C   If this is not a UFD record, ignore it.
      if(rl.lt.54 .or. rtype.ne.2) goto 42

C   Initialise file name and pointer into name
	    name = ' '
      ni = 0
C   Initialise UFD index to point past prefix. This is incremented by
C   48 bytes for each nested directory.
      ufdi = 7
C   Get file type-  0=SAM, 1=DAM, 2=SEGSAM, 3=SEGDAM, 4=Directory
50    ni = ni + 1
      ftype = ichar(buf1(ufdi+39))
	    segdir = .false.
	    if(ftype.gt.1) segdir = .true.
C   Test for segment number (should be for file types 2 & 3 only)
      if(ichar(buf1(ufdi+2)).eq.0) then
	      segdir = .true.
        n4c = buf(ufdi+2:ufdi+5)
        write(name(ni:nl),"('%',i12)") n4
        call rmblnk(name(ni+1:nl),nl-ni)
        goto 54
      endif

C   Remove PRIME parity bits from file name
      call rmbit8(buf1(ufdi+2),32)
C   Add to "name" with preceding "%".
      name(ni:nl) = '%'//buf(ufdi+2:ufdi+33)

54    ni = lnblnk(name)
      ufdi = ufdi + 48
      if(ufdi.lt.rl .and. ni.lt.nl) goto 50

	    call lcase(name,ni)
      if(indx) then
        write(*,"(a)") name(1:ni)
        goto 42
      endif

C   Restore files.

      if(partial) then
C   Check name against required list (% = wildcard)
        do 62 i=1,n
	        j = tnml(i)
	        if(treenm(i)(j:j).eq.'%') j = j - 1
	        if(j.le.0) goto 64
          if(name(1:j).ne.treenm(i)(1:j)) goto 62
	        if(tnml(i).eq.ni) goto 70
	        if(j.ne.tnml(i)) goto 64
62	  continue
      goto 42
      endif

C   Ignore SEG files and directory info  unless "raw" (or "partial") mode.
64	  if(segdir .and. .not.raw) then
        if(ftype.eq.2 .or. ftype.eq.3) write(*,"('not restored:  '
     .                 ,a,' (seg file)')") name(1:ni)
	      goto 42
	    endif

C   Open file on disk.
70    continue
      open(8,file=name(1:ni),status='new',err=110)

	if(.not.(raw .or. segdir)) goto 80

C   Read next record from tape in "raw" mode. Must be data record.
72    call rdrec(tlu,filepos, rn,rl,rtype)
      if(rl.le.6 .or. rtype.ne.1) goto 90

      do 74 i=7,rl
74    ierr = fputc(8,buf1(i))
      if(ierr.ne.0) call perror('(magrst) ')
      goto 72

C   Read MAGSAV records and convert from PRIME text format to SUN ASCII
C   (i.e. remove parity bit and expand blank compressions)
80	  eol = .false.
      blnk = .false.
82	  call rdrec(tlu,filepos, rn,rl,rtype)
      if(rl.le.6 .or. rtype.ne.1) goto 90
	    do 84 i=7,rl
  	    k = ichar(buf1(i))
	      if(blnk) then
  	      do 83 j=1,k
83	        ierr = fputc(8,' ')
	        blnk = .false.
	        goto 84
	      endif
	      if(k.eq.oct221) then
   	      blnk = .true.
	        goto 84
	      endif
	      if(k.ne.0 .or. .not.eol) ierr = fputc(8,char(and(k,mask7)))
   	    eol = .false.
	      if(k.eq.oct212 .and. mod(i,2).ne.0) eol = .true.
84	  continue
      if(ierr.ne.0) call perror('(magrst) ')
      goto 82


C   Close this file and get the next.
90    close(8)
      write(*,"('restored:  ',a)") name(1:ni)
      fn = fn + 1
      if(partial .and. fn.ge.n .and. .not.wildcard) goto 100
      goto 43


100   ierr = 0
      close(tlu)
	    if(fn.eq.0 .and. .not.indx) print *,'No file restored.'
      stop

110   write(*,"('Error opening disk file: ',a)") name(1:ni)
      goto 42

      end

      subroutine rdrec(tlu,filepos,rn,rl,rtype)
C   Read next tape record.
C   On entry:
C     tlu - tape unit, rn - actual record number (incremented on exit)
C   On exit:
C     buf,buf1 - record
C     rl  - record length (0 = EOF, <0 = error)
C     rtype  - record type (1,2,5)

      integer*4 filepos

      integer tlu,rn,rl,rtype,tread,buflen,bufix

      integer*4 bflen
      character*4 bflenc
      equivalence(bflen,bflenc)
      
      integer*4 realreclen
      integer*4 bblen

      character*4096 buf
      character*1 buf1(4096)
      equivalence(buf,buf1)
      common buf

C   Record prefix:  record # - RECN, length - RECL, type - RTYPE
      
      integer*2 recn,recl,rtype1
C      equivalence(recn,buf1),(recl,buf1(3)),(rtype1,buf1(5))
      integer*1 recnhi,recnlo,reclhi,recllo,rtyp1hi,rtyp1lo
      equivalence(recnhi,buf1),(recnlo,buf1(2))
      equivalence(reclhi,buf1(3)),(recllo,buf1(4))
      equivalence(rtyp1hi,buf1(5)),(rtyp1lo,buf1(6))

C   Number of tape read errors
      integer nerr
      data    nerr /0/
      rtype1 = 0

      read(tlu,rec=filepos,err=969) bflen,buf(1:bflen),bblen
      filepos = filepos + bflen + 8
      goto 972
      
969   print *, 'Error trying to read record ', filepos

972   continue
      rl = 0
      if(bflen.eq.bblen) goto 974
      print *, 'Error- record length mismatch',bblen
      stop
      
974   continue 
      rl = bflen     
      rtype1 = rtyp1hi*256 + rtyp1lo
      recn = recnhi*256 + recnlo
      recl = reclhi*256 + recllo
      rtype = rtype1
      if(rl.eq.0) return

C   Check for legal record type (1-5).
      if(rtype.lt.1 .or. rtype.gt.5) then
        print *, 'Wrong record type. (',rtype,')'
        goto 100
      endif

C   Compare actual record length with value in prefix (in bytes)
      recl = recl*2
      rl = recl
C     if(rl.ne.recl) then
C       write(*,"('Inconsistent record length. ')")
C       goto 20
C     endif

C   Compare and then set actual record number to number in prefix
      if(rn.ne.recn) then
        write(*,19) rn,recn
 19     format(' rn=',i8,' recn=',i8)
        write(*,"('Inconsistent record number. ')")
        rn = recn
      endif

      nerr = 0
20    rn = rn + 1
      return


C   Error return. Record should be ignored.
100   rl = -1
      nerr = nerr + 1
      rn = rn + 1
      return
      end

      subroutine rmbit8(buf,n)
C  Removes bit 8 (PRIME parity bit) from character array "buf" of length "n".

      character*1 buf(n)

      do 1 i=1,n
1       buf(i) = char(and(ichar(buf(i)),127))
      return
      end

      subroutine lcase(buf,n)
C  Converts "n" characters in "buf" to lower case.
C  For simplicity this routine also converts '/' to ':' .

      character*1 buf(n)

      do 1 i=1,n
        k = ichar(buf(i))
        if(k.ge.65 .and. k.le.90) buf(i) = char(k+32)
        if(k.eq.47) buf(i) = ':'
1     continue

      return
      end

      subroutine rmblnk(buf,n)
C   Removes leading blanks from character string "buf" of length "n" .

      character*1 buf(n)

      m = 0
1     m = m + 1
      if(buf(m).eq.' ') goto 1
      if(m.eq.1 .or. m.eq.n) return
      i = 0
2     i = i + 1
      buf(i) = buf(m)
      m = m + 1
      if(m.le.n) goto 2
 3    i = i + 1
      buf(i) =  ' '
      if (i.lt.n) goto 3
      return
      end

