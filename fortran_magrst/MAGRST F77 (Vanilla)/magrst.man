


MAGRST(1)                USER COMMANDS                  MAGRST(1)



NAME
     magrst - read PRIME MAGSAV magnetic tapes.

SYNOPSIS
     magrst [irt]

DESCRIPTION
     _m_a_g_r_s_t is a program to read 1600  bpi  magnetic  tape  files
     written  by the PRIME computer's MAGSAV utility.  It expects
     the tape to be mounted on device  _m_t_0 .

     Unless the  "i"  (index)   command  line  option  is  given,
     _m_a_g_r_s_t   interacts  with  the  user  in  a similar manner to
     MAGRST  on the  PRIME.  It first asks the user to  "Enter  a
     logical  tape  number".  The answer to this question is usu-
     ally  1.  However, other numbers could be entered  if  there
     are  more  than  one  "MAGSAV"  on the tape.  Each  "MAGSAV"
     actually occupies three physical tape files,  so  a reply of
     "2"   would  cause  the  first three files on the tape to be
     skipped.

     _m_a_g_r_s_t then displays the tape header information and prompts
     the user thus:

          Ready to restore:

     The legal replies to this are:

     y    means "yes" and the files are  copied  from  the  tape.
          SEG   files  are  omitted unless the  "r"  command line
          option has been  specified.   Also,  without  the   "r"
          option,   all  files  are assumed to be in  PRIME ASCII
          format and so, during copying,  they  will  have  their
          parity bits removed and compressed blanks expanded.

     n    the program exits.

     i    displays an index.  No files are restored.

     r    set "raw" mode.  Everything, including  SEG  files,  is
          then   "restored"   from  tape,  without  modification.
          This option should only be given for binary files.

     p    partial restore.  A list of files to be  restored,  one
          per  line, terminated by a blank line,  should be typed
          in.  File names must be in the same format as shown  by
          the _i_n_d_e_x option.  That is they should begin with a "%"
          sign,  and the  "%"  should  separate  directory  names
          (not  ">"  or  "/" ).

          A  "%"  placed at the end of a  file  name  acts  as  a
          "wildcard"  character.   This  could be used to restore



Sun Release 4.1     Last change: 10 Feb 1989                    1






MAGRST(1)                USER COMMANDS                  MAGRST(1)



          every file in one particular directory.  e.g.

          %dir1%

          would restore all files that were  in  directory   DIR1
          on the PRIME.  Notice that case is not important.


     Files are restored to the current directory on the SUN,  but
     the  original  PRIME  directory  tree  structure  is not re-
     created.  The files are always given lower case names in the
     following format:

          %ddd%ddd%....%fff

     where  "ddd"  represents  an  original  directory  name  and
     "fff"   is  a  filename.   A  separate shell script has been
     written (called _m_k%_d_i_r at Warwick),  which  will  create  an
     appropriate  directory  tree  structure  under  the  current
     directory and move the files into it (see below).

OPTIONS
     i    Write a tape index to the standard output and exit.

     t    Same as  "i"  (for compatibility with  "tar" ).

     r    Set  "raw"  mode as described above.

BUGS
     The program has been tested on rev19 and rev20 MAGSAV tapes,
     but  it may not work on rev21 (or above) tapes if the format
     is changed.

     The following Bourne shell script is required  to  re-create
     the    original   PRIME   directory   structure   from   the
     %ddd%ddd%....%fff  format files that _m_a_g_r_s_t generates.  With
     no  arguments,  the  script  will  search for and "move" all
     files in the current directory  that  begin  with  the   "%"
     character.


          #! /bin/sh
          # Re-creates directory structure for magrst files.

          if [ $# -lt 1 ]; then for i in `ls %*` ;do $0 $i; done
          else
            d=`pwd`
            a=`expr ${1}% : '%\([^%]*\)%.*'`
            b=`expr $1 : '%[^%]*%\(.*\)'`
            while [ $b ]; do
              if [ -d $a ]; then cd $a
              elif [ -f $a ]; then exit 0



Sun Release 4.1     Last change: 10 Feb 1989                    2






MAGRST(1)                USER COMMANDS                  MAGRST(1)



              else mkdir $a; cd $a
              fi
              a=`expr ${b}% : '\([^%]*\)%.*'`
              b=`expr $b : '[^%]*%\(.*\)'`
            done
            if [ -f $a ]; then exit 0; fi
            mv ${d}/$1 $a ; echo $1
          fi




AUTHOR
     Alan Hulme,  Engineering Department,  University of Warwick.









































Sun Release 4.1     Last change: 10 Feb 1989                    3



