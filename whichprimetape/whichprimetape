#!/usr/bin/perl
# whichprimetape, Boone, 04/03/20
# Identify which type of Prime tape this is
#
# Modifications:
# 04/03/20 Boone      Initial coding
# End Modifications

	if (@ARGV)
	{
		foreach $fn (@ARGV)
		{
			open($fh, '<', $fn) ||
				warn "unable to open $fn: $!";
			processtapfile($fh, $fn);
			close(FH);
		}
	}
	else
	{
		processtapfile(\*STDIN, 'STDIN');
	}

	exit(0);

	sub processtapfile
	{
		my ($fh, $fn, $type) = @_;
		my $buf;
		my $ecb;
		my $nc;
		my $obuf;
		my $scb;

		$nc = read($fh, $scb, 4);
		if ($nc != 4)
		{
			# Not enough data in the file to be a tape image
			print STDERR "$fn: apparently not a tape image of any kind\n";
			return;
		}
		$scb = unpack('V', $scb);
		$nc = read($fh, $buf, $scb);
		if ($nc != $scb)
		{
			# Control word calls for more data than in file
			# (Or we just got unlucky and took an i/o error)
			if ($fn eq 'STDIN')
			{
				# Can't do anything about it if read from STDIN
				print STDERR "$fn: not a .tap file, and can't rewind stdin\n";
			}
			else
			{
				# Try reading it as raw
				seek($fh, 0, 0);
				processrawfile($fh, $fn);
			}
		}
		else
		{
			# Sane so far
			$nc = read($fh, $ecb, 4);
			if ($nc != 4)
			{
				# EOF before end ctrl word
				print "$fn: Unknown format\n";
				return;
			}
			$ecb = unpack('V', $ecb);
			if ($ecb != $scb)
			{
				# Busted; try reading it as raw
				seek($fh, 0, 0);
				processrawfile($fh, $fn);
			}
			else
			{
				# Control words match
				$obuf = paroff($buf);
				if		($obuf =~ /^VOL1....................PRIME DRB/)
				{
					$drbtype = drbscan($fh);
					print "$fn: Prime DRB $drbtype\n";
				}
				elsif	($obuf =~ /^VOL1NSB    ARCHIVE/)
				{
					print "$fn: Prime BRMS Archive\n";
				}
				elsif	($obuf =~ /^VOL1NSB    TRANSPORT/)
				{
					print "$fn: Prime BRMS Transport\n";
				}
				# The second word always _seems_ to be 0x000c in the first
				# record, but it's a length so might vary.
				elsif	($obuf =~ /^\000\001\000.\000[\002\004]/)
				{
					$revmap = unpack('n', substr($buf, 6, 2)) & 0160000;
					$rev = '';
					if ($revmap == 0100000) { $rev = ' Rev 12'; }
					if ($revmap == 0140000) { $rev = ' Rev 19'; }
					if ($revmap == 0160000) { $rev = ' Rev 19/ACL'; }
					if ($revmap == 0150000) { $rev = ' Rev 20'; }
					if ($revmap == 0170000) { $rev = ' Rev 20/ACL'; }
					if ($revmap == 0120000) { $rev = ' Rev 22'; }
					if ($revmap == 0130000) { $rev = ' Rev 22/ACL'; }
					print "$fn: Prime MAGSAV$rev\n";
				}
				elsif	($obuf =~ /^\000\000\000\001\001\010/)
				{
					print "$fn: Prime PHYSAV\n";
				}
				else
				{
					print "$fn: unknown format\n";
				}
			}
		}
	}

	sub processrawfile
	{
		my ($fh, $fn) = @_;
		my $buf;
		my $nc;
		my $obuf;

		# Header records VOL1, VOL2, HDR1, HDR2, UHL1, UHL2 at 80
		# bytes each, plus possibly a boot loader tacked into the
		# VOL1, plus at least the first six bytes of the LSR data
		# record, should always be less than 8192 bytes.  Even the
		# Rev24 dist tapes' boot loader is only just over 5000 bytes.
		$nc = read($fh, $buf, 8192);
		$obuf = paroff($buf);
		if		($obuf =~ '^VOL[12]')
		{
			if		($obuf =~ /UHL2.{76}LSR /)
			{
				print "$fn: Raw Prime DRB LSR\n";
			}
			else
			{
				print "$fn: Raw Prime DRB PSR\n";
			}
		}
		# The second word always _seems_ to be 0x000c in the first
		# record, but it's a length so might vary.  Well, one bit
		# me, so now we take any single byte in the low half of the
		# length word.
		elsif	($obuf =~ /^\000\001\000.\000[\002\004]/)
		{
			print "$fn: Raw Prime MAGSAV\n";
		}
		# PHYSAV doesn't have a magic number, just a header with fields
		# for data.  This is for the first block, version 1, first
		# section, header block.  Presumably an army of things that don't
		# match this will now show up.
		elsif	($obuf =~ /^\000\000\000\001\001\010/)
		{
			print "$fn: Raw Prime PHYSAV\n";
		}
		else
		{
			print "$fn: Raw unknown format\n";
		}
	}

	sub drbscan
	{
		my $fh = shift;
		my $buf;
		my $ecb;
		my $nc;
		my $obuf;
		my $scb;

		# Cheep-n-cheezy, assume all reads work

		# vol2 hdr1 hdr2 uhl1 uhl2
		for ($i = 0; $i < 5; $i++)
		{
			$nc = read($fh, $scb, 4);
			$scb = unpack('V', $scb);
			$nc = read($fh, $buf, $scb);
			$nc = read($fh, $ecb, 4);
		}

		# file mark
		$nc = read($fh, $ecb, 4);

		# First data record
		$nc = read($fh, $scb, 4);
		$scb = unpack('V', $scb);
		$nc = read($fh, $buf, $scb);

		$obuf = paroff(substr($buf, 0, 4));
		if ($obuf eq 'LSR ')
		{
			return 'LSR';
		}
		else
		{
			return('PSR');
		}
	}

	sub paroff
	{
		my $str = shift;
		my $i;

		for ($i = 0; $i < length($str); $i++)
		{
			substr($str, $i, 1) =
				chr(ord(substr($str, $i, 1)) & 0x7f);
		}

		return $str;
	}

# Decode a logical save label record

	sub decodelabel
	{
		my ($buf, $debug) = @_;
		my $tclass;
		my $tpdate;
		my $revno;
		my $reel;
		my $lbname;

		if ((unpack('n', substr($buf, 6, 2)) & 0160000) == 0160000)
		{
			$tclass = 'Rev19/ACL' 
		}
		elsif ((unpack('n', substr($buf, 6, 2)) & 0140000) == 0140000)
		{
			$tclass = 'Rev19' 
		}
		elsif ((unpack('n', substr($buf, 6, 2)) & 0100000) == 0100000)
		{
			$tclass = 'Rev12' 
		}
		else
		{
			$tclass = "unkown type " . unpack('n', substr($buf, 6, 2));
		}

		$tpdate = substr($buf, 8, 6);
		$tpdate =~ y/\200-\377/\000-\177/;
		$tpdate =~ s/\s+$//g;
		$tpdate = substr($tpdate, 0, 2) . '/' .
				substr($tpdate, 2, 2) . '/' .
				substr($tpdate, 4, 2);

		$revno = unpack('n', substr($buf, 14, 2));

		$reel = unpack('n', substr($buf, 16, 2));

		$lbname = substr($buf, 18, 6);
		$lbname =~ y/\200-\377/\000-\177/;
		$lbname =~ s/\s+$//g;

		print STDERR "[$lbname - $tclass tape written $tpdate Rev $revno ",
			"Reel $reel]\n";
	}

