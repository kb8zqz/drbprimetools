int   flags,
      fflags = 0,
      sortFlag;					/* Flags for options  */

#define INODE   (flags & 1   )			/* 0000000000000001   */
#define MODE    (flags & 2   )			/* 0000000000000010   */
#define USERID  (flags & 4   )			/* 0000000000000100   */
#define GROUPID (flags & 8   )			/* 0000000000001000   */
#define DEVTYPE (flags & 16  )			/* 0000000000010000   */
#define SIZE    (flags & 32  )			/* 0000000000100000   */
#define DTA     (flags & 64  )			/* 0000000001000000   */
#define DTM     (flags & 128 )			/* 0000000010000000   */
#define DTI     (flags & 256 )			/* 0000000100000000   */
#define SGLCOL  (flags & 512 )			/* 0000001000000000   */
#define NOWAIT  (flags & 1024)			/* 0000010000000000   */
#define DETAIL  (flags & 2048)			/* 0000100000000000   */

#define FILES   (fflags & 1)			/* 0000000000000001   */
#define DIRS    (fflags & 2)			/* 0000000000000010   */
#define HIDDEN  (fflags & 4)			/* 0000000000000100   */

#define SMODE   (sortFlag & 1  )		/* 0000000000000001   */
#define SUSERID (sortFlag & 2  )		/* 0000000000000010   */
#define SGROUP  (sortFlag & 4  )		/* 0000000000000100   */
#define SDEVTYP (sortFlag & 8  )		/* 0000000000001000   */
#define SSIZE   (sortFlag & 16 )		/* 0000000000010000   */
#define SDTA    (sortFlag & 32 )		/* 0000000000100000   */
#define SDTM    (sortFlag & 64 )		/* 0000000001000000   */
#define SDTI    (sortFlag & 128)		/* 0000000010000000   */
#define REVERSE (sortFlag & 256)		/* 0000000100000000   */
