/*
 * LD.C
 * Copyright 1990 (c) Barbary Coast Consulting
 *
 * History
 * Date      Programmer     Comments
 * ??/??/??  S. Hinchey     Original coding
 * 05/17/94  S. Hinchey     Modified to support BSD file systems.
 *                          For SRV5 must define SRV5 for preprocessor
 *                          For BSD  must define BSD  for preprocessor
 * 05/09/95  S. Hinchey     Added support for OSF1 (POSIX)
 *
 *
 * LIMITATIONS
 * 1. Currently the program supports a maximum of 1000 entries in
 *    a directory.  I expect to alter the program in the future 
 *    to use dynamic memory allocation to hold the directory
 *    entries.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>

#if defined(SRV5)
#include <sys/param.h>
#else
#include <sys/dir.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <pwd.h>
#include <grp.h>

#if defined(POSIX) 
#include <dirent.h>
#endif

#define NO_OF_FILES 2000                         /* To size arrays          */
#define TRUE  1                                  /* To make the code easier */
#define FALSE 0                                  /* to read                 */

typedef struct                                   /* Struct for sorting      */
{
#if defined(SRV5)
   char s_name[MAXNAMELEN+1];			         /* Field for sorting  
     */
#else
   char s_name[MAXNAMLEN+1];                     /* Field for sorting       */
#endif
   int  s_ptr;                                   /* Point to unsorted array */
} SARRY;

#include "flags.h"

struct passwd *getpwuid(), *pwentry;
struct group  *getgrgid(), *grentry;

#if defined(POSIX) || defined(BSD)
   struct dirent *(dirArray[NO_OF_FILES+1]);
#else
#ifdef SRV5
   struct direct  dirArray[NO_OF_FILES+1];
#endif
#endif

struct stat statArray[NO_OF_FILES+1];
SARRY       sortArray[NO_OF_FILES+1];


main (argc, argv)
int   argc;
char *argv[];
{
   int indx, pass, count, ptr,                   /* Indexing variables      */
       srtptr,                                   /* Indexing variable       */
       limit,                                    /* Limit of loop           */
       result,                                   /* Result of function      */
       userid, groupid,
       start, done,                              /* Pointer for looping     */
       maxlen,                                   /* Longest filename        */
       wildCard,                                 /* Wildcard in command ?   */
       pathName,
       dirCount,                                 /* Count of file types     */
       hidCount,                                 /* Count of file types     */
       fileCount;                                /* Count of file types     */

   int strchr();

   static long int holdSize,
          li[10] = {1000000000,100000000,        
          10000000,1000000,100000,10000,1000,100,10,1}; /* Long integers    */

   char Ustring[140],                            /* Uppercase string        */
        newdir [140];                            /* Directory being listed  */

#ifdef SRV5
   FILE *fp;
#endif
#if defined(BSD) || defined(POSIX)
   DIR  *dp;
#endif


   void help(),
        dateFix(),
        strUpper(),
        displayFiles();

   int  getline();

/* Initialize some variables */

       srtptr = 0;                               /* Indexing variable       */
       maxlen = 0;                               /* Longest filename        */
       start  =  done = 0;                       /* Pointer for looping     */
       wildCard  = FALSE;                        /* Wildcard in command ?   */
       pathName  = FALSE;
       dirCount  = 0;                            /* Count of file types     */
       hidCount  = 0;                            /* Count of file types     */
       fileCount = 0;                            /* Count of file types     */

   for (indx = 1; indx < argc; indx++)           /* Parse command line      */
   {
      strcpy (Ustring, argv[indx]);              /* Make copy of option     */
      strUpper (Ustring);                        /* Convert to Uppercase    */
      if (Ustring[0] == '-')                     /* Begin with a dash?      */
      {
         if ((strcmp (Ustring, "-H") == 0) ||    /* Asking for help ?       */
             (strcmp (Ustring, "-HELP") == 0))
         {
            help (argv[0]);                      /* Dislay help             */
            return(0);                           /* No error                */
         }
         else if ((strcmp (Ustring, "-INO")   == 0) ||
                  (strcmp (Ustring, "-INODE") == 0))
         {
            flags = flags | 1;
         }
         else if  (strcmp (Ustring, "-MODE")  == 0)
         {
            flags = flags | 2;
         }
         else if ((strcmp (Ustring, "-ID")     == 0) ||
                  (strcmp (Ustring, "-USERID") == 0))
         {
            flags = flags | 4;
            if ((argc > indx+1) &&
			    (argv[indx + 1][0] != '-'))
               userid = atoi (argv[++indx]);
         }
         else if ((strcmp (Ustring, "-GID")     == 0) ||
                  (strcmp (Ustring, "-GROUPID") == 0))
         {
            flags = flags | 8;
            if ((argc > indx+1) &&
				(argv[indx + 1][0] != '-'))
               groupid = atoi (argv[++indx]);
         }
         else if ((strcmp (Ustring, "-DTYPE")    == 0) ||
                  (strcmp (Ustring, "-DEVTYPE")  == 0) ||
                  (strcmp (Ustring, "-DEV_TYPE") == 0))
         {
            flags = flags | 16;
         }
         else if (strcmp (Ustring, "-SIZE")      == 0)
         {
            flags = flags | 32;
         }
         else if ((strcmp (Ustring, "-DTA")                == 0) ||
                  (strcmp (Ustring, "-DATE_TIME_ACCESSED") == 0))
         {
            flags = flags | 64;
         }
         else if ((strcmp (Ustring, "-DTM")                == 0) ||
                  (strcmp (Ustring, "-DATE_TIME_MODIFIED") == 0))
         {
            flags = flags | 128;
         }
         else if ((strcmp (Ustring, "-DTI")             == 0) ||
                  (strcmp (Ustring, "-DATE_TIME_INODE") == 0))
         {
            flags = flags | 256;
         }
         else if ((strcmp (Ustring, "-SGLCOL")        == 0) ||
                  (strcmp (Ustring, "-SINGLE_COLUMN") == 0))
         {
            flags = flags | 512;
         }
         else if ((strcmp (Ustring, "-NW")      == 0) ||
                  (strcmp (Ustring, "-NO_WAIT") == 0))
         {
            flags = flags | 1024;
         }
         else if ((strcmp (Ustring, "-DET")    == 0) ||
                  (strcmp (Ustring, "-DETAIL") == 0))
         {
            flags = 511 + 2048;
         }
         else if ((strcmp (Ustring, "-FILE")  == 0) ||
                  (strcmp (Ustring, "-FILES") == 0))
         {
            fflags = fflags | 1;
         }
         else if ((strcmp (Ustring, "-DIR")  == 0) ||
                  (strcmp (Ustring, "-DIRS") == 0))
         {
            fflags = fflags | 2;
         }
         else if ((strcmp (Ustring, "-HID")    == 0) ||
                  (strcmp (Ustring, "-HIDDEN") == 0))
         {
            fflags = fflags | 4;
         }
         else if ((strcmp (Ustring, "-SORTMO")    == 0) ||
                  (strcmp (Ustring, "-SORT_MODE") == 0))
         {
            sortFlag = sortFlag | 1;
            flags    = flags    | 2;
         }
         else if ((strcmp (Ustring, "-SORTID")      == 0) ||
                  (strcmp (Ustring, "-SORT_USERID") == 0))
         {
            sortFlag = sortFlag | 2;
            flags    = flags    | 4;
         }
         else if ((strcmp (Ustring, "-SORTGID")      == 0) ||
                  (strcmp (Ustring, "-SORT_GROUPID") == 0))
         {
            sortFlag = sortFlag | 4;
            flags    = flags    | 8;
         }
         else if ((strcmp (Ustring, "-SORTDEV")      == 0) ||
                  (strcmp (Ustring, "-SORT_DEVTYPE") == 0))
         {
            sortFlag = sortFlag | 8;
            flags    = flags    | 16;
         }
         else if ((strcmp (Ustring, "-SORTSZ")    == 0) ||
                  (strcmp (Ustring, "-SORT_SIZE") == 0))
         {
            sortFlag = sortFlag | 16;
            flags    = flags    | 32;
         }
         else if ((strcmp (Ustring, "-SORTDTA")                 == 0) ||
                  (strcmp (Ustring, "-SORT_DATE_TIME_ACCESSED") == 0))
         {
            sortFlag = sortFlag | 32;
            flags    = flags    | 64;
         }
         else if ((strcmp (Ustring, "-SORTM")                   == 0) ||
                  (strcmp (Ustring, "-SORTDTM")                 == 0) ||
                  (strcmp (Ustring, "-SORT_DATE_TIME_MODIFIED") == 0))
         {
            sortFlag = sortFlag | 64;
            flags    = flags    | 128;
         }
         else if ((strcmp (Ustring, "-SORTDTI")              == 0) ||
                  (strcmp (Ustring, "-SORT_DATE_TIME_INODE") == 0))
         {
            sortFlag = sortFlag | 128;
            flags    = flags    | 256;
         }
         else if ((strcmp (Ustring, "-REV")     == 0) ||
                  (strcmp (Ustring, "-REVERSE") == 0))
         {
            sortFlag = sortFlag | 256;
         }
         else                                    /* Not a recognized option */
         {
            printf ("Unknown option %s\n", Ustring);
            help (argv[0]);
            return(-1);
         }
      }
      else                                       /* Doesn't begin with "-" */
      {
         if (start == 0)
         {
            wildCard = TRUE;                     /* Wildcarding ?          */
            start = indx;                        /* First file name in args*/
            if (strchr(argv[indx], '/') != 0)
            {
               pathName = TRUE;
               strcpy(newdir, argv[indx]);

#if defined(BSD) || defined(POSIX)
               if ((dp = opendir(newdir))    != NULL) /* Directory ?       */
#else
#ifdef SRV5
               if ((fp = fopen(newdir, "w")) != NULL) /* Directory ?       */
#endif
#endif
               {
                  ptr = strlen(newdir) - 1;      /* No it's a file.  Get    */
                  while (newdir[ptr] != '/')     /* rid of file name.       */
                     newdir[ptr--] = '\0';
                  newdir[ptr] = '\0';
               }
               printf("%s\n", argv[indx]);       /* Display directory name */

#if defined(BSD) || defined(POSIX)
               closedir(dp);
#else
#ifdef SRV5
               fclose (fp);
#endif
#endif
            }
         }
         done = indx + 1;                        /* Last file name in args */
         if (pathName)
         {
            limit = strlen(argv[indx]) - strlen(newdir) - 1;
            for(count=0;count<=limit;count++)
               argv[indx][count] = argv[indx][count+ptr+1];
         }
      }
   }

   if (fflags == 0)                              /* Didn't specify so      */
       fflags =  7;                              /* Show all file types    */

#if defined(BSD) || defined(POSIX)
   if ((dp = opendir ("."))  == NULL) 
#else
#ifdef SRV5
   if ((fp = fopen(".", "r")) == NULL)
#endif
#endif
   {
      perror ("opening directory");
      return(1);  
   }

   limit = 0;

#if defined(BSD) || defined(POSIX)
   while (((dirArray[limit] = readdir(dp)) != NULL)
      && (limit < NO_OF_FILES))
   {
      if (dirArray[limit]->d_ino != 0)            /* Deleted file ?          
*/
#else
#ifdef SRV5
   while (((result = fread (&dirArray[limit], sizeof(dirArray[0]), 1, fp)) > 
0)
      && (limit < NO_OF_FILES))
   {
      if (dirArray[limit].d_ino != 0)             /* Deleted file ?         */
#endif
#endif

      {                                           /* Don't bother with it    
*/
         if (wildCard)                            /* Dealing with wildcard?  
*/
         {
            result = FALSE;
            for (count=start; count<done; count++) /* Search args for match  
*/
            {

#if defined(BSD) || defined(POSIX)
               if (strcmp(argv[count], dirArray[limit]->d_name) == 0)/* 
Match*/
#else
#ifdef SRV5 
               if (strcmp(argv[count], dirArray[limit].d_name) == 0) /* 
Match*/
#endif
#endif

                  result = TRUE;
            }
         }
         else
         {
            result = TRUE;
         }
         if (result)
         {
            sortArray[srtptr].s_ptr = limit;     /* Pointer to array element 
*/

#if defined(BSD) || defined(POSIX)
            result = stat (dirArray[limit]->d_name, &statArray[limit]);
#else
#ifdef SRV5 
            stat (dirArray[limit].d_name, &statArray[limit]);
#endif
#endif

            if (SMODE)
            {
               holdSize = statArray[srtptr].st_mode;
               for (indx = 9, ptr = 0; indx >= 0; indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else if (SUSERID)
            {
               pwentry = getpwuid (statArray[limit].st_uid);
               strcpy(sortArray[srtptr].s_name,pwentry->pw_name);
            }
            else if (SGROUP)
            {
	           grentry = getgrgid (statArray[limit].st_gid);
	           strcpy(sortArray[srtptr].s_name,grentry->gr_name);
            }
            else if (SDEVTYP)
            {
               holdSize = statArray[limit].st_rdev;
               for (indx=9,ptr=0;indx>=0;indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else if (SSIZE)
            {
               holdSize = statArray[limit].st_size;
               for (indx=9,ptr=0;indx>=0;indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else if (SDTA)
            {
               holdSize = statArray[limit].st_atime;
               for (indx=9,ptr=0;indx>=0;indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else if (SDTM)
            {
               holdSize = statArray[limit].st_mtime;
               for (indx=9,ptr=0;indx>=0;indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else if (SDTI)
            {
               holdSize = statArray[limit].st_ctime;
               for (indx=9,ptr=0;indx>=0;indx--)
               {
                  
sortArray[srtptr].s_name[ptr]=((char)(holdSize/li[ptr]))+'a';
                  holdSize -= (long) (sortArray[srtptr].s_name[ptr] * 
li[ptr]);
                  ptr += 1;
               }
               sortArray[srtptr].s_name[ptr] = '\0';
            }
            else
            {
#if defined(BSD) || defined(POSIX)
               strcpy(sortArray[srtptr].s_name, dirArray[limit]->d_name);
#else
#ifdef SRV5
               strcpy(sortArray[srtptr].s_name, dirArray[limit].d_name);
#endif
#endif
            }
            switch(statArray[limit].st_mode & S_IFMT)
            {
               case S_IFDIR: 
                  dirCount += 1;
                  break;
               case S_IFREG: 

#if defined(BSD) || defined(POSIX)
                  if (dirArray[limit]->d_name[0] == '.')
                     hidCount  += 1;
                  else
                     fileCount += 1;
#else
#ifdef SRV5
                  if (dirArray[limit].d_name[0] == '.')
                     hidCount  += 1;
                  else
                     fileCount += 1;
#endif
#endif
                  break;
               case S_IFCHR: 
               case S_IFBLK: 
               case S_IFIFO: 
                  break;
            }
            srtptr += 1;

#if defined(BSD) || defined(POSIX)
            if(strlen(dirArray[limit]->d_name) >= maxlen)
               maxlen = strlen(dirArray[limit]->d_name);
#else
#ifdef SRV5
            if(strlen(dirArray[limit].d_name) >= maxlen)
               maxlen = strlen(dirArray[limit].d_name);
#endif
#endif
            limit += 1;
         }
      }
   }

#if defined(BSD) || defined(POSIX)
   closedir(dp);
#else
#ifdef SRV5
   fclose (fp);
#endif
#endif

/* Now sort the data */
   qsort (sortArray, (size_t) limit, sizeof(SARRY), strcmp);

   if ( flags != 0 )
   {
   for (indx=0; indx< limit; indx++)
#if defined(BSD) || defined(POSIX)
      strcpy(sortArray[indx].s_name, dirArray[sortArray[indx].s_ptr]->d_name);
#else
#ifdef SRV5
      strcpy(sortArray[indx].s_name, dirArray[sortArray[indx].s_ptr].d_name);
#endif
#endif
   }

   if (REVERSE)
   {
      start = limit - 1;
      done  = -1;
   }
   else
   {
      done  = limit;
      start = 0;
   }

   for (pass = 0; pass < 3; pass++)
   {
      switch (pass)
      {
         case 0: 
            if ((fileCount != 0) && (FILES))
            {
               printf ("\n%d Files\n\n", fileCount);
               displayFiles (start, done, pass, maxlen);
            }
            break;
         case 1: 
            if ((dirCount != 0) && (DIRS))
            {
               printf ("\n\n%d Directories\n\n", dirCount);
               displayFiles (start, done, pass, maxlen);
            }
            break;
         case 2: 
            if ((hidCount != 0) && (HIDDEN))
            {
               printf ("\n\n%d Hidden files\n\n", hidCount);
               displayFiles (start, done, pass, maxlen);
            }
            break;
      }
   }
   printf ("\n\n");
   return(0);
}
/* ************************************************************************* 
*/
void displayFiles (start, done, pass, maxlen)
int start, done, pass, maxlen;
{
   int  indx, prnt, lengthLeft, step, result;
   char frmt[6], Ustring[140];                   /* Uppercase string  */

   int  getline();
   void dateFix();

   if (done > start)
      step = 1;
   else
      step = -1;
   maxlen += 3;

#if defined(BSD) || defined(POSIX)
         sprintf(frmt, "%%-%ds", maxlen);
#else
#ifdef SRV5
         sprintf(frmt, "%%%d-s", maxlen);
#endif
#endif

   if (flags != 0 && !(SGLCOL))
   {
      printf(frmt, "Name");
      if (INODE)       printf (" INODE  ");
      if (MODE)        printf (" MODE   ");
      if (USERID)      printf ("USERID      ");
      if (GROUPID)     printf ("GROUPID     ");
      if (DEVTYPE)     printf ("DTYPE   ");
      if (SIZE)        printf ("    SIZE");
      if (DETAIL)
      {
         printf("\n");
         printf(frmt, " ");
      }
      if (DTM)         printf ("DATE TIME MODIFIED    ");
      if (DTA)         printf ("DATE TIME ACCESSED    ");
      if (DTI)         printf ("DATE TIME INODE MOD");
      if (DETAIL);     printf("\n\n");
   }

   lengthLeft = 80;
   for (indx = start; indx != done;)
   {
      prnt = 0;

      switch(statArray[sortArray[indx].s_ptr].st_mode & S_IFMT) 
      {
         case S_IFDIR: 
            if (pass == 1)
               prnt = 1;
            break;
         case S_IFREG: 
           if ((sortArray[indx].s_name[0] == '.') &&
               (pass == 2))
               prnt = 1;
            else if ((sortArray[indx].s_name[0] != '.') &&
					 (pass == 0))
               prnt = 1; 
            break;
         case S_IFCHR: 
         case S_IFBLK: 
         case S_IFIFO: 
            break;
      }
      if (prnt == 1)
      {
         if (maxlen > lengthLeft)
         {
            printf("\n");
            lengthLeft = 80;
         }

#if defined(BSD) || defined(POSIX)
         printf(frmt, sortArray[indx].s_name);  
#else
#ifdef SRV5
         printf(frmt, dirArray[sortArray[indx].s_ptr].d_name); 
#endif
#endif

         lengthLeft -= maxlen;

         if (INODE)
         {
            printf ("%6ld  ", statArray[sortArray[indx].s_ptr].st_ino);
         }
         if (MODE)
         {
            printf ("%6d  ", statArray[sortArray[indx].s_ptr].st_mode);
         }
         if (USERID)
         {
	        pwentry = getpwuid (statArray[sortArray[indx].s_ptr].st_uid);
            if (pwentry != NULL) 
               printf ("%-10s  ", pwentry->pw_name); 
	        else
               printf ("%-10d  ", statArray[sortArray[indx].s_ptr].st_uid);
         }
         if (GROUPID)
         { 
	        grentry = getgrgid (statArray[sortArray[indx].s_ptr].st_gid);
			if (grentry != NULL)
               printf ("%-10s  ", grentry->gr_name);
			else
			   printf ("%-10d  ", 
statArray[sortArray[indx].s_ptr].st_gid);
         }
         if (DEVTYPE)
         {
            if ((S_ISCHR(statArray[sortArray[indx].s_ptr].st_mode)) ||
                (S_ISBLK(statArray[sortArray[indx].s_ptr].st_mode)))
               printf ("%6ld  ", statArray[sortArray[indx].s_ptr].st_rdev);
            else
               printf("%-8s", "<null>");
         }
         if (SIZE)
         {
            if (DETAIL)
            {
               printf ("%10ld\n", statArray[sortArray[indx].s_ptr].st_size); 
               printf(frmt, " ");
            }
            else
               printf ("%10ld  ", statArray[sortArray[indx].s_ptr].st_size); 
         }
         if (DTM)
         {
#if defined(POSIX)
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_mtime));
#else
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_mtime)+4);
#endif
            dateFix (Ustring);
            printf ("%-19s  ", Ustring);
         }
         if (DTA)
         {
#if defined(POSIX)
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_atime));
#else
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_atime)+4);
#endif
            dateFix (Ustring);
            printf ("%-19s  ", Ustring);
         }
         if (DTI)
         {
#if defined(POSIX)
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_ctime));
#else
            sprintf (Ustring, "%s", 
ctime(&statArray[sortArray[indx].s_ptr].st_ctime)+4);
#endif
            dateFix (Ustring);
            printf ("%-19s", Ustring);
         }
         if (DETAIL)
         {
         }
         if (flags != 0)
         {
            printf ("\n");
            lengthLeft = 80;
         }
      }
      indx += step;
   }
}
/* ************************************************************************* 
*/
void dateFix (str)
char  str[];
{
   int   indx, limit;
   char ostr[20], *c;

#if defined(POSIX)
   c = str;
   limit = strlen(c);
   for (indx=4; indx<=limit; indx++)
      ostr[indx-4] = str[indx];
   strcpy(str, ostr);
#endif

   strncpy(ostr, str, 6);
   ostr[6] = '\0';
   strcat (ostr, ", ");
   for (indx=0; indx<4; indx++)
      ostr[indx+8] = str[indx+16];
   for (indx=1; indx<7; indx++)
      ostr[indx+11] = str[indx+5];
   ostr[18] = '\0';
   strcat (ostr, "  ");

   strcpy(str, ostr);
}
/* ************************************************************************* 
*/
void help (string)
char *string;
{
   printf ("\n%s: filename(s) options\n\n", string);
   printf ("Filename is a file or directory about which you are ");
   printf ("requesting information.\n\n"); 
   printf ("Options are:\n\n");
   printf ("-INODE   display the I-node information about this file.\n");
   printf ("-MODE    display the file type.\n");
   printf ("-SIZE    display the size of the file in bytes.\n");
   printf ("-DTA     display the last date-time accessed for this file.\n");
   printf ("-DTM     display the last date-time modified for this file.\n");
   printf ("-DTI     display the last date-time the I-node was changed.\n");
   printf ("-USERID  or -ID    display the userid of the file owner.\n");
   printf ("-GROUPID or -GID   display the groupid of the file.\n");
   printf ("-DEVTYPE or -DTYPE display the type of device if this ");
   printf ("is a special file.\n");
   printf ("-DETAIL  or -DET   display all of the above.\n");
   printf ("-NO_WAIT or -NW    do not pause every 23 lines.\n");
   printf ("-SINGLE_COLUMN or -SGLCOL display each file on a ");
   printf ("separate line.\n\n");
   printf ("-FILES   display only regular files, no directories or ");
   printf ("hidden files.\n");
   printf ("-DIRS    display only directories.\n");
   printf ("-HIDDEN  display only hidden files.\n\n");
}
