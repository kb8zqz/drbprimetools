/*
 * Getline.c
 *
 */

#include <stdio.h>

int getline (s, lim)                              /* Return line and length  
*/
char s[];
int lim;
{
   int c, i, p;

   i = 0;
   p = lim;
   while (--p > 0 && (c = getchar ()) != EOF && c != '\n')
      s[i++] = c;
   s[i] = '\0';
   return (i);
}
