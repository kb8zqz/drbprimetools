/* Strings.c
 *
 * Routines for manipulating strings
 *
 * S. Hinchey
 *
 */

#include <ctype.h>

void strUpper(string)
char string[];
{
   int indx;

   indx = 0;
   while (string[indx] != '\0')
   {
      if (islower(string[indx]))
	     string[indx] = toupper(string[indx]);
	  indx++;
   }
   return;
}

/*****************************************************************************/

void strLower(string)
char string[];
{
   int indx;

   indx = 0;
   while (string[indx] != '\0')
   {
      if (isupper(string[indx]))
	     string[indx] = tolower(string[indx]);
      indx++;
   }
   return;
}

/*****************************************************************************/

char *strstr(str1, str2)
char str1[], str2[];
{
   char *ch1, *ch2;
   int indx;

   indx = 0;
   ch1  = str1;
   ch2  = str2;
   while(ch1)
   {
      if (str1[indx] == str2[0])
      {
	     if (strncmp(ch1,str2,strlen(ch2)) == 0)
	        return(ch1);
      }
      indx++;
      ch1++;
   }
   return((char *) 0);
}

/*****************************************************************************/

void strrev(str)
char str[];
{
   int ptr1, ptr2, limit;
   char c;

   limit = ptr2 = strlen(str);
   ptr2 -= 1;
   limit = (limit + 1) / 2;

   for (ptr1=0,ptr2=limit; ptr1<limit; ptr1++,ptr2--)
   {
      c = str[ptr1];
      str[ptr1] = str[ptr2];
      str[ptr2] = c;
   }
   return;
}

/*****************************************************************************/

void strcrev(in,out)
char in[],
     out[];
{
   int limit,
       ptr1,
       ptr2;

   ptr2 = limit = strlen(in);
   out[ptr2] = '\0';
   for (ptr1=0;ptr1<limit;ptr1++,ptr2--)
      out[ptr2] = in[ptr1];
   return;
}

/*****************************************************************************/

char *substr(str, sub)
char str[],
     sub[];
{
   int indx,
       limit,
       retrn,
       sublen,
       strncmp();

   sublen = strlen(sub);
   limit  = strlen(str) - sublen + 1;
   retrn  = 1;
   indx   = 0;
   while ((indx < limit) && (retrn != 0))
      retrn = strncmp(&str[indx++], sub, sublen);
   if (retrn == 0)
      return (&str[--indx]);
   else
      return ((char) 0);
}

/*****************************************************************************/

char *strrchr(str, chr)
char str[],
     chr;
{
   int indx;
   char *ptr;

   ptr = 0;
   indx = strlen(str) - 1;
   while ((indx >= 0) && (str[indx] != chr))
      indx--;
   if (str[indx] == chr)
      ptr = str+indx;
   return(ptr);
}
