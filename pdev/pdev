#!/usr/bin/perl
# pdev, Boone, 02/15/20
# Calculate PDEVs from command line parameters or decode PDEVs to description
#
# Modifications:
# 02/15/20 Boone      Initial coding
# End Modifications

# Libraries

	use Getopt::Long;

# Initialize

	$help = 0;
	$version = 'new';
	$decode = '';
	$offset = 0;
	$heads = 0;
	$ctrlrstr = '';
	$type = 6;
	$unit = 0;

	@versions = ( 'old', 'mid', 'new' );
	@ctrlraddrs = ( '24', '26', '25', '22', '45', '27', '46', '23' );
	%gopt =
	(
		'help' =>			\$help,
		'version|v=s' =>	\$version,
		'decode=s' =>		\$decode,
		'offset|o=i' =>		\$offset,
		'heads|h=i' =>		\$heads,
		'ctrlr|c=s' =>		\$ctrlrstr,
		'type|t=i' =>		\$type,
		'unit|u=i' =>		\$unit,
	);

# Command line

	GetOptions(%gopt) ||
		exit(22);

	if ($help)
	{
		&help;
	}

	die 'Unknown value for -version'
		unless (&findpos($version, @versions) > -1);

	if ($decode)
	{
		if ($decode !~ /^[0-7]+$/)
		{
			die 'Incorrect PDEV for -decode';
		}
	}
	else
	{
		if (($heads == 0) ||
			($ctrlrstr == ''))
		{
			die 'You must specify -heads and -ctrlr';
		}

		die 'Unknown address for -ctrlr'
			unless (&findpos($ctrlrstr, @ctrlraddrs) > -1);
	}

# Dispatch

	if ($decode)
	{
		&decpdev;
	}
	else
	{
		&encpdev;
	}

# Done

	exit(0);

# Help

	sub help
	{
		while (<DATA>)
		{
			print;
		}
		exit(0);
	}

# Encode PDEVs

	sub encpdev
	{
		if		($version eq 'old')
		{
			$offset /= 2;

			$pdev = 0;

			$pdev |= ($offset & 017) << 12;

			$headshi = ($heads & 036) >> 1;
			$headslo = $heads & 01;
			$pdev |= ($headshi << 8);
			$pdev |= $headslo;

			if ($ctrlrstr eq '27')
			{
				$pdev |= (1 << 7);
			}

			$pdev |= ($type & 017) << 3;

			$pdev |= ($unit & 03) << 1;

			printf("%o\n", $pdev);
		}
		elsif	($version eq 'mid')
		{
			# Pretty sure there was a four controller phase
			# Need to find out which bit was stolen for the 3rd/4th ctrlr
		}
		elsif	($version eq 'new')
		{
			$offset /= 2;

			$pdev = 0;

			$pdev |= ($offset & 017) << 12;

			$headshi = ($heads & 036) >> 1;
			$headslo = $heads & 01;
			$pdev |= ($headshi << 8);
			$pdev |= $headslo;

			$ctrlr = &findpos($ctrlrstr, @ctrlraddrs);
			$pdev |= ($ctrlr << 5);

			$pdev |= (1 << 4);

			$pdev |= ($unit & 03) << 1;

			printf("%o\n", $pdev);
		}
	}

# Decode PDEVs

	sub decpdev
	{
		$decode = oct($decode);

		if		($version eq 'old')
		{
		}
		elsif	($version eq 'mid')
		{
			# Pretty sure there was a four controller phase
			# Need to find out which bit was stolen for the 3rd/4th ctrlr
		}
		elsif	($version eq 'new')
		{
			$offset = (($decode & 0170000) >> 12) * 2;

			$heads = ($decode & 0007400) >> 7;
			$heads |= ($decode & 01);

			$ctrlr = ($decode & 0000340) >> 5;
			$ctrlrstr = $ctrlraddrs[$ctrlr];

			$unit = ($decode & 00000016) >> 1;

			printf("Pdev %o controller '%s offset %d Heads %d Unit %d\n",
				$decode, $ctrlrstr, $offset, $heads, $unit);
		}
	}

# Find position of value in array

	sub findpos
	{
		my ($val, @arr) = @_;

		my $found;
		my $i;

		for ($i = 0; $i < @arr; $i++)
		{
			if ($arr[$i] eq $val)
			{
				return $i;
			}
		}

		return -1;
	}

__DATA__

usage: $0 options...

    -help
    { -version | -v }   { old | new }, default new
    { -offset | -o }    first head in partition (0-7), even, default 0
    { -heads | -h }     number of heads in partition
    { -ctrlr | -c }     octal address of controller
    { -type | -t }      device type, default=6
    { -unit | -u }      unit number, default=0

PDEV bit map

    Bit numbers run from 1 (high order) to 16 (low order).

    Before 4 controller expansion (old)

        1-4    Head offset / 2 (script does this division)
        5-8    Number of surfaces, high order bits
        9      Controller index
        10-13  Device type
        14-15  Unit number
        16     Number of surfaces, low order bit

        Controller index: 0='26 1='27

        Device type:    0=MHD 1=8SPT-FHD 2=Floppy 3=OldCart
                        4=64SPT-FHD 5=MHDorCart 6=SMD

    After 4 controller expansion (mid, not impl)

    After 8 controller expansion (new)

        1-4    Head offset / 2 (script does this division)
        5-8    Number of surfaces, high order bits
        9-11   Controller index
        12     Must be 1
        13-15  Unit number
        16     Number of surfaces, low order bit

        Controller index: 0='24 1='26 2='25 3='22 4='45 5='27 6='46 7='23
        Controllers were usually installed according to the older order,
        i.e. '26, '27, '22, '23.

Disk types

                       Number   Records      Total
     Disk      Model       of       per  Number of    Usable
     Type     Number Surfaces   Surface    Records        MB    Removable
 32MB CMD                2[1]      7407      14814     30.34    1 surface
 64MB CMD                4[2]      7407      29628     60.68    1 surface
 96MB CMD                6[3]      7407      44442     91.02    1 surface

 80MB SMD                5         7407      37035     75.85          yes
300MB SMD       4471    19         7407     140733    288.22          yes

 60MB FMD       4711     4         7140      28560     58.49           no
 68MB FMD                3        10071      30213     61.88           no
 84MB FMD       4714     5         8120      40600     83.15           no
120MB FMD       4715     8         7140      57120    116.98           no
158MB FMD                7        10071      70497    144.38           no
160MB FMD               10         7389      73890    151.33           no
213MB FMD       4730    31         3556      71120    215.30           no
258MB FMD       4719    17         7320     124440    254.85           no
315MB FMD       4475    19         7407     140733    288.22           no
328MB FMD[4]    4721    12        13128     157536    322.63           no
421MB FMD       4731    31         6604     204724    199.85           no
496MB FMD       4735    24         9954     238896    489.26           no
673MB FMD       4729    31        10668     330708    645.91           no
675MB FMD[5]            40         7569     302760    620.05           no
770MB FMD       4835    23        16112     370576    758.94           no
817MB FMD       4860    18        26201     393015    804.89           no
  1GB FMD       4734    31        16510     511810    999.62           no
1.1GB FMD       4935    27        19418     524286   1023.99           no
1.3GB FMD       4732    31        20828     645668   1261.07           no
  2GB FMD       4736    31        30988     960208   1875.40           no

 [1] 1 fixed head at 0, 1 removable at head 21
 [2] 3 fixed head at 0, 1 removable at head 21
 [3] 5 fixed head at 0, 1 removable at head 21
 [4] On 7210 SCSI controller, geometry is 31 heads, 254 sectors, 20 tracks
 [5] aka 600MB
