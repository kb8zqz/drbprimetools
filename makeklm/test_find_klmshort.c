/* test_find_klmd.c, Boone, 09/15/07
   Search files for KLM blocks */

#include <stdio.h>

void fdump(FILE *ih, long offset)
{
	long rc;
	long nr;
	unsigned char buf[272];
	int i;
	int j;
	int target;
	int c;

	rc = fseek(ih, offset, SEEK_SET);
	nr = fread(buf, 272, 1, ih);
	for (i = 0; i < 272; i += 16)
	{
		fputs("        ", stdout);
		for (j = 0; j < 16; j++)
			printf("%02x ", buf[i+j]);
		fputs(" |", stdout);
		for (j = 0; j < 16; j++)
		{
			c = buf[i+j] & 0x7f;
			if (c > 31 && c < 127)
				fputc(c, stdout);
			else
				fputc('.', stdout);
		}
		puts("|");
	}
}

int main(int argc, char *argv[])
{
	int i;
	FILE *ih;
	unsigned int c;
	int state;
	long offset;
	int found;

	for (i = 1; i < argc; i++)
	{
		if ((ih = fopen(argv[i], "rb")) == NULL)
		{
			perror(argv[i]);
			continue;
		}
		state = 0;
		found = 0;
		while ((c = fgetc(ih)) != EOF)
		{
			switch (state)
			{
				case 0:
					if (c == 0x02)
						state = 1;
					continue;
				case 1:
					if (c == 0x88)
					{
						offset = ftell(ih) - 4;
						state = 2;
						printf("%s %08x POSSIBLE 0x0288\n", argv[i], offset);
					}
					else
						state = 0;
					continue;
				case 2:
					if (c == 0x8c)
						state = 3;
					else
						state = 0;
					continue;
				case 3:
					if (c == 0x40)
					{
						offset = ftell(ih) - 4;
						printf("%s %08x\n", argv[i], offset);
						fdump(ih, offset);
						found = 1;
					}
					else
						state = 0;
					state = 0;
					break;
			}
		}
		if (! found)
			printf("%s NO KLM\n", argv[i]);
		fclose(ih);
	}
}
