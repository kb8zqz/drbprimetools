/* makeklm.c, Boone, 05/09/07
   Make a valid Primos KLM structure */

/* Must be compiled with -fpack-struct=2 on gcc/linux-x86 to get 
   correct klm structure alignment.  This is generally very x86
   centric code. */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>
#include <arpa/inet.h>

#define PRODUCT         1
#define NOPRODUCT       2
#define PROTECT         3
#define NOPROTECT       4
#define DELONEXPIRY     5
#define NODELONEXPIRY   6
#define LOGMODIFY       7
#define NOLOGMODIFY     8
#define TYPE            9
#define PRODNAME       10
#define REVISION       11
#define LICENSEE       12
#define SERIALNO       13
#define EXPIRY         14
#define COPYRIGHT1     15
#define COPYRIGHTYEAR  16
#define COPYRIGHT2     17
#define COPYRIGHT3     18
#define PRIMOSREV      19
#define LIBRARYREV     20
#define UCODEREV       21
#define COPYLIMIT      22
#define COPIESMADE     23
#define DASHHELP       24

#define KLMSIZE       136

typedef struct fs_date
{
	unsigned int year : 7;
	unsigned int month : 4;
	unsigned int day : 5;
	unsigned int quadseconds : 15;
} FS_DATE;

typedef struct klm
{
	struct
	{
		unsigned int revision : 8;
		unsigned int size : 8;
	} prv;
	struct
	{
		unsigned int product : 1;
		unsigned int protect : 1;
		unsigned int del_on_expiry : 1;
		unsigned int log_modif : 1;
		unsigned int line1 : 1;
		unsigned int line2 : 1;
		unsigned int type : 4;
		unsigned int spare : 6;
	} keys;
	unsigned short product_name_len;
	unsigned char product_name[20];
	unsigned short revision_len;
	unsigned char revision[20];
	unsigned short licensed_user_len;
	unsigned char licensed_user[40];
	unsigned short serial_number_len;
	unsigned char serial_number[20];
	unsigned long expiry_date;
	struct
	{
		unsigned short notice_1_len;
		unsigned char notice_1[14];
		unsigned short year_len;
		unsigned char year[14];
		unsigned short notice_2_len;
		unsigned char notice_2[50];
		unsigned short notice_3_len;
		unsigned char notice_3[20];
	} copyright_notice;
	struct
	{
		unsigned short primos_base_rev_len;
		unsigned char primos_base_rev[10];
		unsigned short library_base_rev_len;
		unsigned char library_base_rev[10];
		unsigned short ucode_base_rev_len;
		unsigned char ucode_base_rev[10];
	} rev_dependencies;
	struct
	{
		unsigned int st_copy_limit : 8;
		unsigned int st_copies_made : 8;
	} st_product_copies;
	short st_user_limit;
	short st_spare_wd;
	unsigned long rd_checksum;
	unsigned long nr_pointer;
} KLM;

void help()
{
	fprintf(stderr, "usage: makeklm [options]\n"
	"\t--product, --noproduct\n"
	"\t--protect, --noprotect\n"
	"\t--delonexpiry, --nodelonexpiry\n"
	"\t--logmodify, --nologmodify\n"
	"\t--type int\n"
	"\t--prodname string(20)\n"
	"\t--revision string(20)\n"
	"\t--licensee string(40)\n"
	"\t--serialno string(20)\n"
	"\t--expiry date (as unix time() value)\n"
	"\t--copyright1 string(14)\n"
	"\t--copyrightyear int\n"
	"\t--copyright2 string(50)\n"
	"\t--copyright3 string(20)\n"
	"\t--primosrev string(10)\n"
	"\t--libraryrev string(10)\n"
	"\t--ucoderev string(10)\n"
	"\t--copylimit int\n"
	"\t--copiesmade int\n"
	"\t--help\n");
}

	const struct option opts[] =
	{
		{ "product",       0, NULL, PRODUCT },
		{ "noproduct",     0, NULL, NOPRODUCT },
		{ "protect",       0, NULL, PROTECT },
		{ "noprotect",     0, NULL, NOPROTECT },
		{ "delonexpiry",   0, NULL, DELONEXPIRY },
		{ "nodelonexpiry", 0, NULL, NODELONEXPIRY },
		{ "logmodify",     0, NULL, LOGMODIFY },
		{ "nologmodify",   0, NULL, NOLOGMODIFY },
		{ "type",          1, NULL, TYPE },
		{ "prodname",      1, NULL, PRODNAME },
		{ "revision",      1, NULL, REVISION },
		{ "licensee",      1, NULL, LICENSEE },
		{ "serialno",      1, NULL, SERIALNO },
		{ "expiry",        1, NULL, EXPIRY },
		{ "copyright1",    1, NULL, COPYRIGHT1 },
		{ "copyrightyear", 1, NULL, COPYRIGHTYEAR },
		{ "copyright2",    1, NULL, COPYRIGHT2 },
		{ "copyright3",    1, NULL, COPYRIGHT3 },
		{ "primosrev",     1, NULL, PRIMOSREV },
		{ "libraryrev",    1, NULL, LIBRARYREV },
		{ "ucoderev",      1, NULL, UCODEREV },
		{ "copylimit",     1, NULL, COPYLIMIT },
		{ "copiesmade",    1, NULL, COPIESMADE },
		{ "help",          0, NULL, DASHHELP },
		{ 0,                 0, 0,    0 }
	};

/* year modulo 100, except 100-128 mean 2000-2028 */
/* month 1-12 */
/* day 1-31 */
/* quadseconds 4-second groups elapsed after midnight */

void cvdate(int unixdate, FS_DATE *primosdate)
{
	struct tm *gmt;

	gmt = gmtime((const time_t *)&unixdate);
	if (gmt -> tm_year > 128)
		primosdate -> year = gmt -> tm_year % 100;
	else
		primosdate -> year = gmt -> tm_year;
	primosdate -> month = gmt -> tm_mon + 1;
	primosdate -> day = gmt -> tm_mday;
	primosdate -> quadseconds = ((gmt -> tm_hour * 3600) + (gmt -> tm_min * 60) +
		gmt -> tm_sec) / 4;
}

void chvarcpy(unsigned char *dest, unsigned short *destlen,
	unsigned char *src, int len)
{
	unsigned char *c;

	c = src;
	while (*c)
		*c++ |= 0x80;
	strncpy((void *)dest, (void *)src, len);
	*destlen = htons(len);
}

int main(int argc, char *argv[])
{
	int rc;
	KLM outs;
	FS_DATE fsd;
	time_t unixdate;
	int i;
	unsigned long *ptr;
	unsigned long cksum;

	/* Structure packing debug helps */

/*
	printf("prv %p\n", &outs.prv);
	printf("keys %p\n", &outs.keys);
	printf("product_name_len %p product_name %p\n",
		&outs.product_name_len, &outs.product_name);
	printf("revision_len %p revision %p\n",
		&outs.revision_len, &outs.revision);
	printf("licensed_user_len %p licensed_user %p\n",
		&outs.licensed_user_len, &outs.licensed_user);
	printf("serial_number_len %p serial_number %p\n",
		&outs.serial_number_len, &outs.serial_number);
	printf("expiry_date %p\n", &outs.expiry_date);
	printf("copyright_notice %p { notice_1_len %p notice_1 %p year_len "
		"%p year %p notice_2_len %p notice_2 %p notice_3_len %p "
		"notice_3 %p }\n", &outs.copyright_notice,
		&outs.copyright_notice.notice_1_len,
		&outs.copyright_notice.notice_1,
		&outs.copyright_notice.year_len,
		&outs.copyright_notice.year,
		&outs.copyright_notice.notice_2_len,
		&outs.copyright_notice.notice_2,
		&outs.copyright_notice.notice_3_len,
		&outs.copyright_notice.notice_3);
	printf("rev_dependencies %p { primos_base_rev_len %p "
		"primos_base_rev %p library_base_rev_len %p "
		"library_base_rev %p ucode_base_rev_len %p "
		"ucode_base_rev %p }\n", &outs.rev_dependencies,
		&outs.rev_dependencies.primos_base_rev_len,
		&outs.rev_dependencies.primos_base_rev,
		&outs.rev_dependencies.library_base_rev_len,
		&outs.rev_dependencies.library_base_rev,
		&outs.rev_dependencies.ucode_base_rev_len,
		&outs.rev_dependencies.ucode_base_rev);
	printf("st_product_copies %p\n", &outs.st_product_copies);
	printf("st_user_limit %p\n", &outs.st_user_limit);
	printf("st_spare_wd %p\n", &outs.st_spare_wd);
	printf("rd_checksum %p\n", &outs.rd_checksum);
	printf("nr_pointer %p\n", &outs.nr_pointer);
 */

	/* Initialize */

	if (sizeof(outs) != KLMSIZE*2)
	{
		fprintf(stderr, "klm structure size is %d, not %d\n",
			sizeof(outs), KLMSIZE*2);
		exit(EINVAL);
	}

	memset(&outs, 0, KLMSIZE*2);
	outs.prv.revision = 2;
	outs.prv.size = KLMSIZE;

	/* Parse the command line */

	while ((rc = getopt_long_only(argc, argv, "", (void *)&opts, NULL)) != -1)
	{
		switch(rc)
		{
			case '?':
				break;
			case PRODUCT:
				outs.keys.product = 1;
				break;
			case NOPRODUCT:
				outs.keys.product = 0;
				break;
			case PROTECT:
				outs.keys.protect = 1;
				break;
			case NOPROTECT:
				outs.keys.protect = 0;
				break;
			case DELONEXPIRY:
				outs.keys.del_on_expiry = 1;
				break;
			case NODELONEXPIRY:
				outs.keys.del_on_expiry = 0;
				break;
			case LOGMODIFY:
				outs.keys.log_modif = 1;
				break;
			case NOLOGMODIFY:
				outs.keys.log_modif = 0;
				break;
			case TYPE:
				outs.keys.type = htons(atoi(optarg));
				break;
			case PRODNAME:
				chvarcpy(outs.product_name, &outs.product_name_len,
					(unsigned char *)optarg, 20);
				break;
			case REVISION:
				chvarcpy(outs.revision, &outs.revision_len,
					(unsigned char *)optarg, 20);
				break;
			case LICENSEE:
				chvarcpy(outs.licensed_user, &outs.licensed_user_len,
					(unsigned char *)optarg, 20);
				break;
			case SERIALNO:
				chvarcpy(outs.serial_number, &outs.serial_number_len,
					(unsigned char *)optarg, 20);
				break;
			case EXPIRY:
				unixdate = atoi(optarg);
				cvdate(unixdate, &fsd);
				memcpy(&outs.expiry_date, &fsd, 4);
				break;
			case COPYRIGHT1:
				chvarcpy(outs.copyright_notice.notice_1,
					&outs.copyright_notice.notice_1_len,
					(unsigned char *)optarg, 14);
				outs.keys.line1 = 1;
				break;
			case COPYRIGHTYEAR:
				chvarcpy(outs.copyright_notice.year,
					&outs.copyright_notice.year_len,
					(unsigned char *)optarg, 4);
				break;
			case COPYRIGHT2:
				chvarcpy(outs.copyright_notice.notice_2,
					&outs.copyright_notice.notice_2_len,
					(unsigned char *)optarg, 50);
				outs.keys.line2 = 1;
				break;
			case COPYRIGHT3:
				chvarcpy(outs.copyright_notice.notice_3,
					&outs.copyright_notice.notice_3_len,
					(unsigned char *)optarg, 20);
				break;
			case PRIMOSREV:
				chvarcpy(outs.rev_dependencies.primos_base_rev,
					&outs.rev_dependencies.primos_base_rev_len,
					(unsigned char *)optarg, 10);
				break;
			case LIBRARYREV:
				chvarcpy(outs.rev_dependencies.library_base_rev,
					&outs.rev_dependencies.library_base_rev_len,
					(unsigned char *)optarg, 10);
				break;
			case UCODEREV:
				chvarcpy(outs.rev_dependencies.ucode_base_rev,
					&outs.rev_dependencies.ucode_base_rev_len,
					(unsigned char *)optarg, 10);
				break;
			case COPYLIMIT:
				outs.st_product_copies.st_copy_limit =
					htons(atoi((void *)optarg));
				break;
			case COPIESMADE:
				outs.st_product_copies.st_copies_made =
					htons(atoi((void *)optarg));
				break;
			case DASHHELP:
				help();
				exit(0);
			default:
				fprintf(stderr, "unknown option code %d\n", rc);
				exit(EINVAL);
		}
	}

	/* Set the checksum */

	ptr = (void *)&outs;
	i = 65;
	cksum = 0L;
	while (i > -1)
	{
		cksum = cksum ^ *(ptr+i);
		i--;
	}
	outs.rd_checksum = cksum;

	/* Output the block */

	fwrite(&outs, 1, KLMSIZE*2, stdout);

	/* Done */

	exit(0);
}
