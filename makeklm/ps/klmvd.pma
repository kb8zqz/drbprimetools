* KLM$VD.PMA, PRIMOS>R3S, PRIMOS GROUP, 03/01/89
* Validate a serialization data area.
* Copyright (c) 1986, Prime Computer, Inc., Natick, MA 01760
*                     All Rights Reserved
*
* Description:
*
*        dcl klm$vd entry (ptr options(short)) returns (bit(1) aligned);
*
*        klm_data_okay = klm$vd(addr(klm_data));
*
*  Given a pointer to a klm data area, this routine will validate the
*  information in that area.  If the serialization data in that area is
*  okay, this routine will return true ('1'b).
*
* Abnormal conditions:
*
* Implementation:
*  This routine validates the data area version number, the size of the
*  area, and the checksum values in the area.
*
*  This routine is coded in PMA, rather than SPL, because the checksum
*  is computed using the exclusive OR function, which is unavailable in
*  SPL.
*
* Modifications:
*   Date   Programmer     Description of modification
* 03/01/89 Cook           Pick up ptr via LDL, not LDA!
* 12/23/88 Cook/Lowry     Initial coding.
*
*
         SEG
         RLIT
         SYML

         SUBR KLM$VD

         DYNM KLMPTR(3)
         DYNM CHKOFF(1)

* Start out by validating the KLM data pointer.

KLMVAL   ARGT
         LDL  KLMPTR,*
         BLT  BADKLM         Return false if a faulted pointer.
         ANA  NULLPTR
         ERL  NULLPTR
         BLEQ BADKLM         Return false if a null pointer.

* Validate the KLM data area rev number and data sizes.
*
* Valid rev numbers are: 1, 2
* The only valid data size for either rev is 136 halfwords, despite the
* fact that a rev 1 structure is really 134 halfwords long if you look
* at the structure definition.
*
* The checksum is computed on the first 130 halfwords in either case.
* It does not include the last 6 halfwords of the data, partly because
* this is where the stored checksum is kept.
*
* The offset of the checksum in the data structure differs, depending
* on the rev: 130 if rev 1, 132 if rev 2.
*
         EAXB KLMPTR,*       KLM data pointer to XB%.
         EAXB XB%,*

         LDA  XB%+0          Retrieve version #/size word.
         SUB  =(1.LS.8)+136  Version = 1/size = 136?
         BNE  NOTVER1
         LDA  =130           Yes, offset of checksum data is 130.
         JMP  VEROKAY

NOTVER1  SUB  =((2.LS.8)+136)-((1.LS.8)+136)    Version = 2/size = 136?
         BNE  BADKLM
         LDA  =132           Yes, offset of checksum data is 132.

VEROKAY  STA  CHKOFF         Save required data area size.

* Version and size are okay.
* Recompute the checksum values and compare them to the stored values.
*
* The KLM software computes the checksum for the even and odd halfwords
* separately, but we can do it on a 32 bit word basis.

         LDX  =130           Data area size is always the first 130 halfwords.

         CRL                 Init. accumulated checksum to zero.
LOOP     ERL  XB%-2,X        Loop computing checksum on pairs of halfwords
         DRX
         BDX  LOOP

         LDX  CHKOFF         Check our computed checksum against the stored
         ERL  XB%,X          value which is always located here relative to
         BLNE BADKLM         the size of the data area.

OKAYKLM  LDA  ='100000       Everything is okay, return a PL1 true.
         PRTN

BADKLM   CRA                 KLM data is bad, for some reason.  Return false.
         PRTN

NULLPTR  DATA '7777, 0

         LINK

KLM$VD   ECB  KLMVAL,,KLMPTR,1

         END
