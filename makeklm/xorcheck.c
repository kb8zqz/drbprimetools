#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	unsigned long buf;
	unsigned long cksum;
	int rc;
	int ofs;
	FILE *fh;

	fh = fopen(argv[1], "rb");
	if (fh == NULL)
		exit(1);
	cksum = 0L;
	ofs = 256;
	while (ofs > -1)
	{
		fseek(fh, ofs, SEEK_SET);
		rc = fread(&buf, 1, 4, fh);
		cksum = cksum ^ buf;
		ofs -= 4;
	}
	printf("checksum is %08x\n", htonl(cksum));
}
