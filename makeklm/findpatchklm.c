/* findpatchklm.c, Boone, 09/15/07
   Search files for KLM blocks, patch licensee, serialno */

#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>

#define KLMLENCHAR	272

#define LICENSEE	1
#define SERIALNO	2
#define DASHHELP	3

const struct option opts[] =
{
	{ "licensee",	1, NULL, LICENSEE },
	{ "serialno",	1, NULL, SERIALNO },
	{ "help",		0, NULL, DASHHELP },
	{ 0, 0, 0, 0 }
};

void chvarcpy(unsigned char *dest, unsigned short *destlen,
	unsigned char *src, int len)
{
	unsigned char *c;

	c = src;
	while (*c)
		*c++ |= 0x80;
	strncpy((void *)dest, (void *)src, len);
	*destlen = htons(len);
}

void fdump(FILE *ih, long offset, char *msg)
{
	long rc;
	long nr;
	unsigned char buf[KLMLENCHAR];
	int i;
	int j;
	int target;
	int c;

	puts(msg);
	rc = fseek(ih, offset, SEEK_SET);
	nr = fread(buf, KLMLENCHAR, 1, ih);
	for (i = 0; i < KLMLENCHAR; i += 16)
	{
		fputs("        ", stdout);
		for (j = 0; j < 16; j++)
			printf("%02x ", buf[i+j]);
		fputs(" |", stdout);
		for (j = 0; j < 16; j++)
		{
			c = buf[i+j] & 0x7f;
			if (c > 31 && c < 127)
				fputc(c, stdout);
			else
				fputc('.', stdout);
		}
		puts("|");
	}
}

void clear(unsigned char *buf, unsigned short buflen)
{
	while (buflen)
	{
		buf[buflen--] = ' ';
	}
}

void patch(FILE *ih, long offset,
	int licensee_flag, unsigned short licensee_len, unsigned char *licensee,
	int serialno_flag, unsigned short serialno_len, unsigned char *serialno)
{
	long rc;
	long nr;
	unsigned char buf[KLMLENCHAR];
	int i;
	unsigned long *ptr;
	unsigned long cksum;

	rc = fseek(ih, offset, SEEK_SET);
	nr = fread(buf, KLMLENCHAR, 1, ih);

	if (licensee_flag)
	{
		memcpy(&buf[48], &licensee_len, 2);
		memcpy(&buf[50], licensee, 40);
	}
	if (serialno_flag)
	{
		memcpy(&buf[90], &serialno_len, 2);
		memcpy(&buf[92], serialno, 20);
	}

	ptr = (void *)buf;
	i = 65;
	cksum = 0L;
	while (i > -1)
	{
		cksum = cksum ^ *(ptr+i);
		i--;
	}
	memcpy(&buf[264], &cksum, 4);

	rc = fseek(ih, offset, SEEK_SET);
	nr = fwrite(buf, 272, 1, ih);
}

int main(int argc, char *argv[])
{
	int rc;
	int licensee_flag = 0;
	unsigned short licensee_len = 0;
	unsigned char licensee[40] = "                                        ";
	int serialno_flag = 0;
	unsigned short serialno_len = 0;
	unsigned char serialno[20] = "                    ";
	int i;
	FILE *ih;
	unsigned int c;
	int state;
	long offset;
	int found;

/* Parse the command line */

	while ((rc = getopt_long_only(argc, argv, "", (void *)&opts, NULL)) != -1)
	{
		switch(rc)
		{
			case '?':
				break;
			case LICENSEE:
				chvarcpy(licensee, &licensee_len, (unsigned char *)optarg,
					strlen(optarg));
				licensee_flag = 1;
				break;
			case SERIALNO:
				chvarcpy(serialno, &serialno_len, (unsigned char *)optarg,
					strlen(optarg));
				serialno_flag = 1;
				break;
			default:
				fprintf(stderr, "unknown option code %d\n", rc);
				exit(EINVAL);
		}
	}

/* Process all files named on the command line */

	for (i = optind; i < argc; i++)
	{
		if ((ih = fopen(argv[i], "rb+")) == NULL)
		{
			perror(argv[i]);
			continue;
		}
		state = 0;
		found = 0;
		while ((c = fgetc(ih)) != EOF)
		{
			switch (state)
			{
				case 0:
					if (c == 0x02)
						state = 1;
					continue;
				case 1:
					if (c == 0x88)
						state = 2;
					else
						state = 0;
					continue;
				case 2:
					if (c == 0x8c)
						state = 3;
					else
						state = 0;
					continue;
				case 3:
					if (c == 0x40)
					{
						offset = ftell(ih) - 4;
						printf("%s %08x\n", argv[i], offset);
						fdump(ih, offset, "Pre-patch:");
						patch(ih, offset, licensee_flag, licensee_len,
							licensee, serialno_flag, serialno_len, serialno);
						fdump(ih, offset, "Post-patch:");
						found = 1;
					}
					else
						state = 0;
					state = 0;
					break;
			}
		}
		if (! found)
			printf("%s NO KLM\n", argv[i]);
		fclose(ih);
	}

/* Done */

	exit(0);
}
