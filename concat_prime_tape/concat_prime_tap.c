/* concat_prime_tap.c, Boone, 03/07/07
   Concatenate .tap files containing Prime MAGSAV+friends saves
   Copyright (C) 2007, Dennis Boone, East Lansing, MI */

/* Modifications:
   03/07/07 Boone      Initial coding
   End Modifications */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <arpa/inet.h>

#define MAXFNS 32
#define MAXSTR 1024
#define MAXTAPEIO 65536

unsigned int readrec(FILE *fh, unsigned char *buf)
{
	unsigned int reclen;
	unsigned int nr;

	nr = fread(&reclen, 1, 4, fh);
	if (nr == 0)
	{
		perror("physical eof without eof mark");
		return(0xffffffff);
	}
	if (nr != 4)
	{
		perror("reading pre reclen");
		exit(EIO);
	}

	if (reclen == 0xffffffff)
		return(reclen);

	if ((reclen & 0x80000000) == 0x80000000)
		return(reclen);

	if (reclen == 0)
		return(reclen);

	nr = fread(buf, 1, reclen, fh);
	if (nr != reclen)
	{
		perror("reading record");
		exit(EIO);
	}

	nr = fread(&reclen, 1, 4, fh);
	if (nr != 4)
	{
		perror("reading post reclen");
		exit(EIO);
	}

	return(reclen);
}

void writerec(char *buf, unsigned int reclen)
{
	unsigned int nw;

	nw = fwrite(&reclen, 1, 4, stdout);
	if (nw != 4)
	{
		perror("writing pre reclen");
		exit(EIO);
	}

	if ((reclen & 0x80000000) == 0x80000000)
		return;

	nw = fwrite(buf, 1, reclen, stdout);
	if (nw != reclen)
	{
		perror("writing record");
		exit(EIO);
	}

	if (reclen != 0)
	{
		nw = fwrite(&reclen, 1, 4, stdout);
		if (nw != 4)
		{
			perror("writing post reclen");
			exit(EIO);
		}
	}
}

unsigned short process(char *infn, int i, int baserecnum)
{
	FILE *fh;
	int labelhack;
	unsigned int reclen;
	unsigned char buf[MAXTAPEIO];
	unsigned short rectyp;
	unsigned short recnum;

	if ((fh = fopen(infn, "rb")) == NULL)
	{
		perror(infn);
		return(0);
	}

	labelhack = 0;
	reclen = 0;
	while (reclen != 0xffffffff)
	{
		memset(buf, 0, MAXTAPEIO);
		reclen = readrec(fh, buf);
		if (reclen != 0xffffffff)				/* Not physical eov */
		{
			if (reclen == 0x00000000)			/* file mark */
			{
				if (labelhack)
				{
					writerec(buf, reclen);
					labelhack = 0;
				}
			}
			else
			{
				memcpy(&rectyp, &buf[4], 2);
				rectyp = ntohs(rectyp);
				memcpy(&recnum, buf, 2);
				recnum = ntohs(recnum);
				recnum += baserecnum;			/* Fix up magsr seqno */
				recnum = htons(recnum);
				if (rectyp == 4)				/* Label needs fm after */
				{
					if (i == 0)
					{
						labelhack = 1;
						writerec(buf, reclen);
					}
				}
				else
				{
					memcpy(buf, &recnum, 2);	/* Only if not label */
					writerec(buf, reclen);
				}
			}
		}
	}

	fclose(fh);

	return(ntohs(recnum));
}

void capoff()
{
	unsigned int filemark = 0x00000000;
	unsigned int eod = 0xffffffff;

	fwrite(&filemark, 1, 4, stdout);
	fwrite(&filemark, 1, 4, stdout);
	fwrite(&eod, 1, 4, stdout);
}

int main(int argc, char *argv[])
{
	int i;
	unsigned char ifns[32][MAXSTR+1];
	unsigned char nfns = 0;
	int baserecnum = 0;

	for (i = 1; i < argc; i++)
	{
		strncpy(ifns[nfns], argv[i], MAXSTR);
		nfns++;
	}

	for (i = 0; i < nfns; i++)
	{
		baserecnum = process(ifns[i], i, baserecnum);
	}

	capoff();

	exit(0);
}
