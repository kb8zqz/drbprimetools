# PrimosDisk.pm, Boone, 10/07/19
# Disk/filesystem access library for p50em type disk image files
#
# Modificattions:
# 10/07/19 Boone      Initial coding
# End Modifications

	package PrimosDisk;

	use strict;
	use warnings;

	use Carp;
	use Exporter;
	use Fcntl qw(:seek);

	use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

	our $VERSION     = 1.00;
	our @ISA         = qw(Exporter);
	our @EXPORT      = ();
	our @EXPORT_OK   = qw(getrec nextrec prevrec parentrec);
	our %EXPORT_TAGS = (ALL => [qw(getrec nextrec prevrec parentrec)]);

	our $rechdrunpkl = 'N N n n N N n';
	our $rechdrunpks = 'n n n n n n n';
	our $rathdrunpk19 = 'n n N n n n n';
	our $rathdrunpk20 = 'n n N n n n n n';
	our $rathdrunpk21 = 'n n N n n n n n n n N N N';

	my @geom =
	(
		# model, fnsuffix, heads, spt, maxtrack
		[ 1,  "80m",  5,   9,  823, ],
		[ 1, "300m", 19,   9,  823, ],
		[ 2,  "cmd", 21,   9,  823, ],
		[ 4,  "68m",  3,   9, 1120, ],
		[ 5, "158m",  7,   9, 1120, ],
		[ 6, "160m", 10,   9,  822, ],
		[ 7, "675m", 40,   9,  842, ],
		[ 7, "600m", 40,   9,  842, ],
		[ 9, "315m", 19,   9,  823, ],  # MODEL_4475
		[ 10,  "84m",  5,   8, 1016, ], # MODEL_4714
		[ 11,  "60m",  4,   7, 1020, ], # MODEL_4711
		[ 12, "120m",  8,   7, 1020, ], # MODEL_4715
		[ 13, "496m", 24,  14,  712, ], # MODEL_4735
		[ 14, "258m", 17,   6, 1221, ], # MODEL_4719
		[ 15, "770m", 23,  19,  850, ], # MODEL_4845
		[ 16, "1.1g", 27,  19, 1022, ], # MODEL_4935
		[ 17, "328a", 12,   8, 1641, ], # MODEL_4721
		[ 17, "328b", 31, 254,   20, ], # MODEL_4721 (7210 SCSI controller)
		[ 18, "817m", 15,  19, 1381, ], # MODEL_4860
		[ 19, "673m", 31, 254,   42, ], # MODEL_4729
		[ 20, "213m", 31, 254,   14, ], # MODEL_4730
		[ 22, "421m", 31, 254,   26, ], # MODEL_4731
		[ 23, "1.3g", 31, 254,   82, ], # MODEL_4732
		[ 24, "1g",   31, 254,   65, ], # MODEL_4734
		[ 25, "2g",   31, 254,  122, ], # MODEL_4736
	);

	my @rechdrftypes =
	(
		'SAM',
		'DAM',
		'SEGSAM',
		'SEGDAM',
		'PASUFD',
		'ACLUFD',
		'ACAT',
		'CAM',
	);
	my @ecwftypes =
	(
		'OldUFDhdr',
		'NewUFDhdr',
		'Vacant',
		'File',
		'ACAT',
		'ACL',
		'DirIdxBlk',
	);
	my @filinfotypes =
	(
		'SAM',
		'DAM',
		'SEGSAM',
		'SEGDAM',
		'PasUFD',
		'ACLUFD',
		'ACAT',
		'CAM',
	);
	my @rwlocktypes =
	(
		'Sys',
		'Excl',
		'Updt',
		'None',
	);

	use constant BYTESPERWORD => 2;
	use constant RATRECNO     => 2;

	sub new
	{
		my $class = shift;
		my %args =
		(
			imgfile => '',			# File name of disk image
			partition => 1,			# 1-based ordinal of partition desired
			skipheads => 0,			# future: skip heads before scan
			@_
		);

		my $self = {};

		my $i;
		my $found;
		my $geomname;
		my $ratcyls;
		my $ratdsiz;
		my $ratflags;
		my $rathdrlen;
		my $ratnhds;
		my $ratrecsiz;
		my $ratrev;

		$self -> {'imgfile'} = $args{'imgfile'};
		$self -> {'partition'} = $args{'partition'};

		$geomname = lc(substr($args{'imgfile'},
			index($args{'imgfile'}, '.')+1));
		$found = 0;
		for ($i = 0; $i < @geom; $i++)
		{
			if ($geom[$i][1] eq $geomname)
			{
				$self -> {'geomheads'} = $geom[$i][2];
				$self -> {'geomsectors'} = $geom[$i][3];
				$self -> {'geomcyls'} = $geom[$i][4];
				$found = 1;
			}
		}
		if (! $found)
		{
			croak('new: no known disk type suffix in imagefile name');
		}

		# Initialize this to a value sufficient for fetching the first
		# RAT header, so the math in calcrecoffset works the first time
		$self -> {'ratnhds'} = 1;

		open($self -> {'fh'}, '+<', $args{'imgfile'}) ||
			croak('new: unable to open image file: ' . $!);
		$self -> {'diskrecsize'} = findrecsize($self) + 16;

		# This partition discovery code depends on partitions on the
		# drive image starting on head 0, and being contiguous down
		# as far as the desired partition.  Having a non-split paging
		# partition in between file systems will also likely result
		# in badness.  In the future, we may support a "skip heads"
		# option to new() to skip over initial unused heads.  We do
		# try to skip garbage, at great cost to the readability of
		# this section of code.
		$self -> {'partoff'} = 0;
		$i = 0;
		while ($i < $self -> {'partition'})
		{
			$i++;
			($rathdrlen, $ratrecsiz, $ratdsiz, $ratnhds, $ratflags,
				$ratcyls, $ratrev) = getrathdr($self);
			if (($ratnhds < 1) ||
				($ratnhds > 31) ||
				(($ratnhds + $self -> {'partoff'}) >
					$self -> {'geomheads'}) ||
				($rathdrlen < 8) ||
				($rathdrlen > 34) ||
				($ratrev < 0) ||
				($ratrev > 5))
			{
				# Can't start on an odd # head, so skip 2 heads
				$self -> {'partoff'} += 2;
				$ratnhds = -1;
			}
			else
			{
				$self -> {'partoff'} += $ratnhds
					if ($i < $self -> {'partition'});
			}
		}
		if (($ratnhds < 1) || ($ratnhds > 31))
		{
			$ratnhds = 0;
		}
		$self -> {'ratnhds'} = $ratnhds;

		return bless $self, $class;
	}

	sub findrecsize
	{
		my $self = shift;

		my $datcnt;
		my $nr;
		my $rechdr;

		seek($self -> {'fh'}, 8, SEEK_SET) ||
			croak('findrecsize: unable to seek to datcnt: ' . $!);
		$nr = read($self -> {'fh'}, $rechdr, 2);
		$nr == 2 ||
			croak('findrecsize: unable to read datcnt: ' . $!);
		$datcnt = unpack('n', $rechdr);
		if		($datcnt == 1024)
		{
			return 1024;
		}
		elsif	($datcnt == 440)
		{
			return 440;
		}
		else
		{
			croak('findrecsize: unknown disk record header size/version');
		}
	}

	sub getrathdr
	{
		my $self = shift;

		my $record;
		my $rathdrlen;
		my $ratrecsiz;
		my $ratdsiz;
		my $ratnhds;
		my $ratflags;
		my $ratcyls;
		my $ratrev;

		getrec($self, 2);
		($rathdrlen, $ratrecsiz, $ratdsiz, $ratnhds, $ratflags,
			$ratcyls, $ratrev) =
			unpack($rathdrunpk19, substr($self -> {'record'}, 32));
		return($rathdrlen, $ratrecsiz, $ratdsiz, $ratnhds, $ratflags,
			$ratcyls, $ratrev);
	}

	sub calcrecoffset
	{
		my ($self, $recno) = @_;
		my $cylinder;
		my $drivehead;
		my $lba;
		my $offset;
		my $ptnhead;
		my $scratch;
		my $sector;

		# CHS within partition

		$cylinder = int($recno /
			int($self -> {'geomsectors'} * $self -> {'ratnhds'}));

		$scratch = $recno %
            ($self -> {'geomsectors'} * $self -> {'ratnhds'});
		$ptnhead = int($scratch / $self -> {'geomsectors'});

		$sector = $recno % $self -> {'geomsectors'};
		
		# CHS within drive

		$drivehead = $ptnhead + $self -> {'partoff'};

		# CHS -> LBA

		$lba = ($cylinder * $self -> {'geomheads'} * $self -> {'geomsectors'}) +
			($drivehead * $self -> {'geomsectors'}) +
			$sector;

		# LBA -> bytes in file

		$offset = $lba * $self -> {'diskrecsize'} * 2;

		return($offset);
	}

	sub fillheader
	{
		my $self = shift;

		my $bra;
		my $count;
		my $cra;
		my $level;
		my $next;
		my $prev;
		my $type;

		if ($self -> {'diskrecsize'} == 1040)
		{
			($cra, $bra, $count, $type, $next, $prev, $level) =
				unpack($rechdrunpkl, $self -> {'record'});
		}
		elsif ($self -> {'diskrecsize'} == 448)
		{
			($cra, $bra, $count, $type, $next, $prev, $level) =
				unpack($rechdrunpks, $self -> {'record'});
		}
		$self -> {'hdrcra'} = $cra;
		$self -> {'hdrbra'} = $bra;
		$self -> {'hdrcount'} = $count;
		$self -> {'hdrtype'} = $type;
		$self -> {'hdrnext'} = $next;
		$self -> {'hdrprev'} = $prev;
		$self -> {'hdrlevel'} = $level;
	}

	sub gethdrcra { my $self = shift; return $self -> {'hdrcra'}; }
	sub gethdrbra { my $self = shift; return $self -> {'hdrbra'}; }
	sub gethdrcount { my $self = shift; return $self -> {'hdrcount'}; }
	sub gethdrtype { my $self = shift; return $self -> {'hdrtype'}; }
	sub gethdrnext { my $self = shift; return $self -> {'hdrnext'}; }
	sub gethdrprev { my $self = shift; return $self -> {'hdrprev'}; }
	sub gethdrlevel { my $self = shift; return $self -> {'hdrlevel'}; }

	sub getrec
	{
		my ($self, $recno) = @_;

		my $nr;
		my $off;

		$off = calcrecoffset($self, $recno);
		seek($self -> {'fh'}, $off, SEEK_SET) ||
			croak('getrec: unable to seek to record: ' . $!);
		$nr = read($self -> {'fh'}, $self -> {'record'},
			($self -> {'diskrecsize'} * BYTESPERWORD));
		$nr == ($self -> {'diskrecsize'} * BYTESPERWORD) ||
			croak('getrec: unable to read record: ' . $!);
		$self -> {'recno'} = $recno;
		fillheader($self);

		return $self -> {'record'};
	}

	sub nextrec
	{
		my $self = shift;

		if ($self -> {'recno'} < $self -> {'partnrecs'})
		{
			$self -> {'recno'}++;
			return getrec($self, $self -> {'recno'});
		}
		else
		{
			croak('nextrec: logic error');
		}
	}

	sub prevrec
	{
		my $self = shift;

		if ($self -> {'recno'} > 0)
		{
			$self -> {'recno'}--;
			return getrec($self, $self -> {'recno'});
		}
		else
		{
			croak('prevrec: logic error');
		}
	}

	sub parentrec
	{
		my $self = shift;

		my $cra;
		my $bra;
		my $count;
		my $type;
		my $next;
		my $prev;
		my $level;

		return getrec($self, $bra);
	}

	sub dumpdiskrechdr
	{
		my ($self, $record) = @_;

		my $bra;
		my $count;
		my $cra;
		my $level;
		my $next;
		my $prev;
		my $type;
		my $typemask;
		my $typestr;

		if ($self -> {'diskrecsize'} == 1040)
		{
			($cra, $bra, $count, $type, $next, $prev, $level) =
				unpack($rechdrunpkl, $self -> {'record'});
		}
		elsif ($self -> {'diskrecsize'} == 448)
		{
			($cra, $bra, $count, $type, $next, $prev, $level) =
				unpack($rechdrunpks, $self -> {'record'});
		}
		$typemask = $type & 077777;
		$typestr = $rechdrftypes[$typemask];
		$typestr .= ' Special' if ($type != $typemask);

		print STDERR <<"EOF";
           Record#: $cra
    Parent record#: $bra
        Data words: $count
      Next record#: $next
      Prev record#: $prev
         File type: $typestr
    DAM file level: $level

EOF
	}

	sub getufd
	{
		my ($self, $recno, $path) = @_;

		my $ufd;

		$self -> {'ufdra'} = $recno;
		while (1)
		{
#print STDERR "ufd $path recno $recno\n";
			getrec($self, $recno);
#print STDERR "rec hdr count ", $self -> {'hdrcount'} * 2, "\n";
#print STDERR "length of record ", length($self -> {'record'}), "\n";
			$ufd .= substr($self -> {'record'}, 32,
				$self -> {'hdrcount'} * 2);
#print STDERR "length of ufd ", length($ufd), "\n";
			$recno = $self -> gethdrnext;
			last if ($recno == 0);
		}

		$self -> {'ufd'} = $ufd;
		return $ufd;
	}

	sub getent
	{
		my ($self, $offset) = @_;

		my $ecwtype;
		my $ecwsize;

		($ecwtype, $ecwsize) = unpack('C C',
			substr($self -> {'ufd'}, $offset, 2));
#print STDERR "getent ecw type $ecwtype size $ecwsize\n";
		$self -> {'ent'} = substr($self -> {'ufd'}, $offset, ($ecwsize*2));
		$offset += (2*$ecwsize);
		$self -> {'entoffset'} = $offset;

		return $offset;
	}

	sub getentbra
	{
		my $self = shift;

		return unpack('x2 N', $self -> {'ent'});
	}

	sub getentname
	{
		my $self = shift;

		my $ecwtyp;
		my $ecwsiz;
		my $filnam;
		my $namlen;
		my $unpkfmt;

		($ecwtyp, $ecwsiz) = unpack('C C', $self -> {'ent'});
		if ($ecwtyp == 1 || $ecwtyp == 6)
		{
			return '';
		}

		($namlen) = unpack('x22 n', $self -> {'ent'});
		$namlen--;
		$namlen *= 2;
		$unpkfmt = 'x24 a' . $namlen;
		($filnam) = unpack($unpkfmt, $self -> {'ent'});
		$filnam = $self -> fixstr($filnam);
		$filnam = '(empty)' if (! $filnam);
		$filnam =~ s/\s+$//g;

		return $filnam;
	}

	sub getenttype
	{
		my $self = shift;

		my $ecwtyp;
		my $ecwsiz;

		($ecwtyp, $ecwsiz) = unpack('C C', $self -> {'ent'});

		return $ecwtyp;
	}

	sub getufdsize
	{
		my $self = shift;

		return length($self -> {'ufd'});
	}

	sub fixstr
	{
		my ($self, $str) = @_;

		my $i;
		my $c;
		my $newstr;

		$newstr = '';
		for ($i = 0; $i < length($str); $i++)
		{
			$c = ord(substr($str, $i, 1)) & 0x7f;
			if ($c > 0x1f && $c < 0x7f)
			{
				$newstr .= chr(ord(substr($str, $i, 1)) & 0x7f);
			}
		}

		return $newstr;
	}

	sub entisufd
	{
		my ($self) = shift;

		my $ecw;
		my $filtyp;

		$ecw = unpack('C', $self -> {'ent'});
		return 1==0 if ($ecw == 2);
		$filtyp = unpack('x21 C', $self -> {'ent'});

		return ($filtyp == 4 || $filtyp == 5);
	}

	sub dumpufdhdr
	{
		my ($self, $path) = @_;

		my $ecwtyp;
		my $ecwsiz;
		my $ownpw;
		my $nonpw;
		my $maxquo;
		my $dirusd;
		my $treusd;
		my $rtprod;
		my $proddtm;
		my $freepos;
		my $hashver;
		my $hashsiz;

		($ecwtyp, $ecwsiz) = unpack('C C', $self -> {'ent'});
		$self -> {'ufdtyp'} = $ecwtyp;
		if		($ecwtyp == 0)		# old ufd header
		{
			($ownpw, $nonpw, $maxquo, $dirusd, $treusd, $rtprod, $proddtm) =
				unpack('x2 A6 A6 x2 N N N N N',
				$self -> {'ent'});
			$ownpw = $self -> fixstr($ownpw);
			$nonpw = $self -> fixstr($nonpw);
			$nonpw = '      ' if (! $nonpw);
			printf("ufd %s at ra %d\n" .
				"\towner password [%-6s] non-owner password [%-6s]\n" .
				"\tmax quota [%d]  q used in dir [%d]  q used in tree [%d]\n" .
				"\trecord-time product [%d]  dtm-record product [%d]\n",
				$path, $self -> {'ufdra'}, $ownpw, $nonpw, $maxquo, $dirusd,
				$treusd, $rtprod, $proddtm);
		}
		elsif	($ecwtyp == 1)		# new ufd header
		{
			($ownpw, $nonpw, $maxquo, $dirusd, $treusd, $rtprod,
				$proddtm, $freepos, $hashver, $hashsiz) =
				unpack('x2 A6 A6 x2 N N N N N n n n',
				$self -> {'ent'});
			$ownpw = $self -> fixstr($ownpw);
			$nonpw = $self -> fixstr($nonpw);
			$nonpw = '      ' if (! $nonpw);
			printf("ufd %s at ra %d\n" .
				"\towner password [%6s] non-owner password [%6s]\n" .
				"\tmax quota [%d]  q used in dir [%d]  q used in tree [%d]\n" .
				"\trecord-time product [%d]  dtm-record product [%d]\n" .
				"\tfree position [%d]  hash version [%d]  hash size [%d]\n",
				$path, $self -> {'ufdra'}, $ownpw, $nonpw, $maxquo,
				$dirusd, $treusd, $rtprod, $proddtm, $freepos,
				$hashver, $hashsiz);
		}
	}

	sub dumpufdent
	{
		my ($self, $path) = @_;

		my $ecwtyp;
		my $ecwsiz;
		my $namlen;
		my $unpkfmt;
		my $bra;
		my $logtyp;
		my $dtb;
		my $protec;
		my $aclpos;
		my $dtm;
		my $infobits;
		my $filtyp;
		my $filnam;
		my $dtc;
		my $dtl;
		my $link;

		($ecwtyp, $ecwsiz) = unpack('C C', $self -> {'ent'});
		if		($ecwtyp == 2 || $ecwtyp == 6)	# vacant, dir index block
		{
			printf("vacant or idx entry - ecw type [%s] at offset [%d]\n",
				$ecwftypes[$ecwtyp], $self -> {'entoffset'});
			return;
		}
		elsif	($ecwtyp == 4 || $ecwtyp == 5)	# acat, acl
		{
			printf("acat or acl entry - ecw type [%s] at offset [%d]\n",
				$ecwftypes[$ecwtyp], $self -> {'entoffset'});
			return;
		}
		else									# file
		{
			if		($self -> {'ufdtyp'} == 0)
			{
			}
			if		($self -> {'ufdtyp'} == 1)
			{
				($namlen) = unpack('x22 n', $self -> {'ent'});
				$namlen--;
				$namlen *= 2;
				$unpkfmt = 'x2 N n N n n N C C x2 a' . $namlen . ' N N n';
				($bra, $logtyp, $dtb, $protec, $aclpos, $dtm, $infobits,
					$filtyp, $filnam, $dtc, $dtl, $link) =
					unpack($unpkfmt, $self -> {'ent'});
#print STDERR "data shortage\n" if (length($self -> {'ent'}) < (34 + $namlen));
				$filnam = $self -> fixstr($filnam);
				$filnam = '(empty)' if (! $filnam);
				$filnam =~ s/\s+$//g;
				printf("entry %s ecw type [%s] at offset [%d] link [%d]\n" .
					"\trwlock [%s] info bits [%s]\n" .
					"\tbra [%d] aclpos [%d]\n" .
					"\tlogtyp [%d] filtyp [%s] protec [%s]\n" .
					"\tmodified [%s]\n" .
					"\tcreated [%s]\n" .
					"\taccessed [%s]\n",
					$path . '>' . $filnam, $ecwftypes[$ecwtyp],
					$self -> {'entoffset'}, $link,
					$self -> decoderwl($infobits),
					$self -> decodeinfo($infobits), $bra, $aclpos, $logtyp,
					$filinfotypes[$filtyp], $self -> decodeprotec($protec),
					$self -> formatdate($self -> decodefsdate($dtm)),
					$self -> formatdate($self -> decodefsdate($dtc)),
					$self -> formatdate($self -> decodefsdate($dtl)));
			}
		}
	}

	sub decoderwl
	{
		my ($self, $infobits) = @_;

		my $rwl;

		$rwl = ($infobits & 0x0c) >> 2;
		return $rwlocktypes[$rwl];
	}

	sub decodeinfo
	{
		my ($self, $infobits) = @_;

		my @accum;

		push(@accum, 'Long RAT') if ($infobits & 0x80);
		push(@accum, 'Dumped') if ($infobits & 0x40);
		push(@accum, 'DOS mod') if ($infobits & 0x20);
		push(@accum, 'Special') if ($infobits & 0x10);
		push(@accum, 'Truncated') if ($infobits & 0x02);

		return join(', ', @accum);
	}

	sub decodeprotec
	{
		my ($self, $protec) = @_;

		my $owner;
		my $nonowner;

		$owner = $nonowner = '';

		$owner .= 'R' if ($protec & 0x0100);
		$owner .= 'W' if ($protec & 0x0200);
		$owner .= 'D' if ($protec & 0x0400);

		$nonowner .= 'R' if ($protec & 0x01);
		$nonowner .= 'W' if ($protec & 0x02);
		$nonowner .= 'D' if ($protec & 0x04);

		return 'owner=' . $owner . ', non-owner=' . $nonowner;
	}

	sub formatdate
	{
		my ($self, $year, $month, $day, $hour, $min, $sec) = @_;

		return sprintf('%02d-%02d-%02d %02d:%02d:%02d',
			$year, $month, $day, $hour, $min, $sec);
	}

	sub decodefsdate
	{
		my ($self, $fsdate) = @_;

		my $year;
		my $month;
		my $day;
		my $qsec;
		my $timemod;
		my $hour;
		my $min;
		my $sec;

		$year = (($fsdate & 0xfe000000) >> 25) + 1900;
		$month = ($fsdate & 0x01e00000) >> 21;
		$day = ($fsdate & 0x001f0000) >> 16;
		$qsec =($fsdate & 0x0000ffff) * 4;
		$hour = int($qsec / 3600);
		$timemod = $qsec % 3600;
		$min = int($timemod / 60);
		$sec = $timemod % 60;

		return($year, $month, $day, $hour, $min, $sec);
	}

	1;
