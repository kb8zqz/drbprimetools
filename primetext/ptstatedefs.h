/* ptstatedefs.h, Boone, 11/07/11
   State table defines for primetext */

/* Modifications:
   11/07/11 Boone      Initial coding
   12/16/11 Boone      Newline word pad char may be space, apparently
   End Modifications */

#define Any    -1

#define Prev   -1

#define Odd    1
#define Even   2

#define Yes    1
#define No     2

#define Emit   1
#define Red    2
#define Unred  3
#define Eat    4
#define Sub    5
#define Super  6
#define Bitch  7
#define Copy   8
#define Spaces 9
#define Unsub  10
#define Unsup  11
#define Feeds  12

#define NUL    0
#define NL     0212
#define SO     0216
#define SI     0217
#define DLE    0220
#define DC1    0221
#define DC2    0222
#define DC3    0223
#define DC4    0224
#define SPC    0240
