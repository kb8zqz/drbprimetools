/* ptstate.c, Boone, 11/14/11
   FSA for primetext */

/* Modifications:
   11/14/11 Boone      Initial coding
   End Modifications */

#include "ptstatedefs.h"
#include "ptstate.h"

int getstate(int state, int evenodd, int c, int htmlout,
	int *xlate, int *action, int *nextstate)
{
	int i;

	for (i = 0; ptstate[i].state != -2; i++)
	{
		if ((ptstate[i].state == state) &&
			((ptstate[i].evenodd == evenodd) ||
				(ptstate[i].evenodd == Any)) &&
			((ptstate[i].charin == c) ||
				(ptstate[i].charin == Any)) &&
			((ptstate[i].htmlout == htmlout) ||
				(ptstate[i].htmlout == Any)))
		{
			*xlate = ptstate[i].translate;
			*action = ptstate[i].action;
			*nextstate = ptstate[i].nextstate;
			return(0);
		}
	}
	return(1);
}
