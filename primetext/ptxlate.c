/* ptxlate.c, Boone, 11/14/11
   Character translation table for primetext */

/* Modifications:
   11/14/11 Boone      Initial coding
   End Modifications */

#include "ptxlatedefs.h"
#include "ptxlate.h"

int getxlate(int c, int *c2)
{
	*c2 = charaction[c].outchar;
	return(charaction[c].action);
}
