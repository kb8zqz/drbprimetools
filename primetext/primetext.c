/* primetext.c, Boone, 11/07/11
   Turn Prime ASCII/ECS into utf8 text or html */

/* Modifications:
   11/07/11 Boone      Initial coding
   End Modifications */

/* Description:

   "Prime ASCII/ECS" has the following characteristics:

   1.  The high order bit is set for all normal ASCII characters.
   2.  Extended characters have the high order bit unset.
   3.  If a newline falls in an odd-numbered byte (the top half of
       a 16-bit word), the following byte will be a null, and is
       ignored.
   4.  A control-P followed by a one byte integer 'n' indicates a form
       of compression where 'n' bytes are copied from the current
       position in the previous line to the current line, replacing
       the control-P sequence.  How this interacts with the copy,
       space or newline compression features is unknown.  The program
       assume that such things don't co-occur.
   5.  A control-Q followed by a one byte integer 'n' indicates that
       'n' spaces should be inserted in the current line, replacing
       the control-Q sequence.
   6.  A control-S followed by a one byte integer 'n' indicates that
       'n' line feeds should be inserted at the current position,
       replacing the control-S sequence.
   7.  In some hardware output contexts, a control-N may cause a red
       ribbon shift.
   8.  In some hardware output contexts, a control-O may cause a black
       ribbon shift.
   9.  In some hardware output contexts, a control-R may cause a half
       line forward movement (i.e. succeeding characters will be
       output as subscripts, or the effect of a preceding control-T
       will be undone).
   10. In some hardware output contexts, a control-T may cause a half
       line backward movement (i.e. succeeding characters will be
       output as superscripts, or the effect a preceding control-R
       will be undone).

   The program assumes that ribbon color change section boundaries
   and half-line movement section boundaries do not overlap.  This
   is probably safe since the machines which did ribbon color changes
   (teletypes, TermiNets, etc.) did not support half line movements.

   Author's note:
   I have not seen compression types 4 and 6 above in the wild.  They
   must have fallen out of use before Rev. 18.1 since the kernel source
   for that version includes only space compression (type 5 above).
   Only very early Prime documentation attests to their existence.
   Please send me any examples that you find!

   By default, primetext will:

   1.  Map ASCII/ECS characters per 1-3 above to UTF-8.
   2.  Expand compression per 4-6 above.
   3.  Output text.

   Command line options allow:

   1.  Selecting HTML output.
   2.  Enabling translation of items 7-10 above if output mode is HTML.
   3.  Stripping control characters not translated per the above.

   End Description */

#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>

#define USAGE "usage: %s [--html|-h] [--devicespecific|-d] [--stripctl|-s]\n" \
              "          [<filenames>]\n"
#define SHORTOPTS "hds"

void processfile(FILE *, char *, int, int, int);

static struct option lopts[] =
{
	"html",				0,	NULL,	'h',	/* Output HTML */
	"devicespecific",	0,	NULL,	'd',	/* Format device-specific codes */
	"stripctl",			0,	NULL,	's',	/* Strip control characters */
	0, 0, 0, 0
};

int main(int argc, char *argv[])
{
	int c;									/* Return from getopt */
	int optidx;								/* Return from getopt */
	int html = 0;							/* Output html */
	int devspec = 0;						/* Format device-specific codes */
	int stripctl = 0;						/* Strip control characters */
	int i;									/* Loop index */
	FILE *ifh;								/* Input file handle */

/* Initialize */

/* Command line */

	while (1)
	{
		c = getopt_long(argc, argv, SHORTOPTS, lopts, &optidx);
		if (c == -1)
			break;
		switch (c)
		{
			case 'h':
				html = 1;
				break;
			case 'd':
				devspec = 1;
				break;
			case 's':
				stripctl = 1;
				break;
			default:
				fprintf(stderr, USAGE, argv[0]);
				exit(EINVAL);
		}
	}

/* Process files */

	if (optind < argc)						/* Files named */
		for (i = optind; i < argc; i++)
		{
			if ((ifh = fopen(argv[i], "rb")) == NULL)
			{
				perror(argv[i]);
				continue;
			}
			processfile(ifh, argv[i], html, devspec, stripctl);
			fclose(ifh);
		}
	else									/* Filter stdin */
	{
		processfile(stdin, "stdin", html, devspec, stripctl);
	}

/* Done */

	exit(0);
}
