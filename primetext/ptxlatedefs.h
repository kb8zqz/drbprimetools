/* ptxlate.h, Boone, 11/07/11
   Translation table defines for primetext */

/* Modifications:
   11/07/11 Boone      Initial coding
   End Modifications */

#define ACT_XLATE 1
#define ACT_DELCC 2
