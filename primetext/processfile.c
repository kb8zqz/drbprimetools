/* processfile.c, Boone, 11/13/11
   Core character processing loop for primetext */

/* Modifications:
   11/13/11 Boone      Initial coding
   End Modifications */

#include <sys/types.h>
#include <stdio.h>
#include "ptstatedefs.h"
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "ptxlatedefs.h"

#define DEBUG     0
#define MAXLINE   32768
#define HTMLHEAD  "<html><head><title>%s</title></head><body><pre>"
#define HTMLTAIL  "</pre></body></html>\n"
#define HTMLRED   "<font color=\"#ff0000\">"
#define HTMLUNRED "</font>"
#define HTMLSUP   "<sup>"
#define HTMLUNSUP "</sup>"
#define HTMLSUB   "<sub>"
#define HTMLUNSUB "</sub>"

int getstate(int, int, int, int, int *, int *, int *);
int getxlate(int, int *);
int toutf8(char *, int , u_int32_t *, int);

void processfile(FILE *ifh, char *fn, int htmlout, int devspec, int stripctl)
{
	int prevstate;							/* FSA condition stack */
	int state;								/* FSA condition */
	int evenodd;							/* Even or odd byte in word */
	unsigned char curline[MAXLINE];			/* Compression ref buffer */
	unsigned char prevline[MAXLINE];		/* Compression ref buffer */
	int linepos;							/* Index into output buffer */
	int c;									/* Byte from file */
	int rc;									/* Return code from various */
	int xlate;								/* Value from FSA table */
	int action;								/* Value from FSA table */
	int nextstate;							/* Value from FSA table */
	int xlateact;							/* From charaction table */
	int c2;									/* Translated char */
	unsigned char s[8];						/* UTF8 bytestring */
	int i;									/* Loop index */

	/* Initialize */

	prevstate = 0;
	state = 0;
	evenodd = Odd;
	memset(curline, 0, MAXLINE);
	memset(prevline, 0, MAXLINE);
	linepos = 0;

	if (htmlout)
		fprintf(stdout, HTMLHEAD, fn);

	/* Process characters */

	while ((c = fgetc(ifh)) != EOF)
	{
		if (DEBUG)
			fprintf(stderr, "c %d state %d evenodd %d htmlout %d"
				" prevstate %d\n", c, state, evenodd, htmlout, prevstate);
		rc = getstate(state, evenodd, c, htmlout,
			&xlate, &action, &nextstate);
		if (DEBUG)
			fprintf(stderr, "  xlate %d action %d nextstate %d\n",
				xlate, action, nextstate);
		if (rc)
		{
			fprintf(stderr, "\n!! no entry for state %d evenodd %d c %d "
				"htmlout %d devspec %d stripctl %d\n", state, evenodd,
				c, htmlout, devspec, stripctl);
			exit(EINVAL);
		}
		xlateact = getxlate(c, &c2);
		if (DEBUG)
			fprintf(stderr, "  xlateact %d c2 %d\n", xlateact, c2);
		if (xlate)
		{
			toutf8(s, sizeof(s), &c2, 1);
		}
		switch (action)
		{
			case Emit:
				if (xlateact != ACT_DELCC)
				{
					strncat(curline, s, MAXLINE - strlen(curline));
					linepos += strlen(s);
					if (c == 0212)
					{
						memcpy(prevline, curline, strlen(curline));
						memset(curline, 0, MAXLINE);
						linepos = 0;
					}
					fputs(s, stdout);
				}
				break;
			case Red:
				fputs(HTMLRED, stdout);
				break;
			case Unred:
				fputs(HTMLUNRED, stdout);
				break;
			case Eat:
				/* chomp chomp mmm tasty */
				break;
			case Sub:
				fputs(HTMLSUB, stdout);
				break;
			case Super:
				fputs(HTMLSUP, stdout);
				break;
			case Bitch:
				fprintf(stderr, "\n!! discarding non-NULL character in even"
					" byte after a newline in file %s near offset %ld\n",
					fn, ftell(ifh));
				break;
			case Copy:
				if (c+linepos <= strlen(prevline))
					fwrite(prevline+linepos, 1, c, stdout);
				break;
			case Spaces:
				if (DEBUG)
					fprintf(stderr, "  outputting %d spaces\n", c);
				for (i = 0; i < c; i++)
					fputc(' ', stdout);
				break;
			case Unsub:
				fputs(HTMLUNSUB, stdout);
				break;
			case Unsup:
				fputs(HTMLUNSUP, stdout);
				break;
			case Feeds:
				for (i = 0; i < c; i++)
					fputc(0012, stdout);
				memset(prevline, 0, MAXLINE);
				memset(curline, 0, MAXLINE);
				linepos = 0;
				break;
			default:
				fprintf(stderr, "\n!! unrecognized action %d in file %s"
				" near offset %ld\n", action, fn, ftell(ifh));
				exit(EINVAL);
		}

		if (nextstate == Prev)
			state = prevstate;
		else
		{
			prevstate = state;
			state = nextstate;
		}
		evenodd++;
		if (evenodd > Even)
			evenodd = Odd;
	}

	/* Clean up */

	if (htmlout)
		fputs(HTMLTAIL, stdout);
}

