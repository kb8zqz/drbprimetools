/* ptstate.h, Boone, 11/07/11
   State table for primetext */

/* Modifications:
   11/07/11 Boone      Initial coding
   12/16/11 Boone      Newline word pad char may be space, apparently
   End Modifications */

struct
{
	int state;
	int evenodd;
	int charin;
	int translate;
	int action;
	int nextstate;
	int htmlout;
}
ptstate[] =
{
/*           State  EvenOdd CharIn  Translate Action  Next  HTMLout */
/* start */
             0,     Odd,    NL,     Yes,      Emit,   1,    Any,
             0,     Any,    SO,     No,       Red,    2,    Yes,
             0,     Any,    SI,     No,       Eat,    0,    Yes,
             0,     Any,    DLE,    No,       Eat,    3,    Any,
             0,     Any,    DC1,    No,       Eat,    4,    Any,
             0,     Any,    DC2,    No,       Sub,    5,    Yes,
             0,     Any,    DC3,    No,       Eat,    6,    Any,
             0,     Any,    DC4,    No,       Super,  7,    Yes,
             0,     Any,    Any,    Yes,      Emit,   0,    Any,
/* nl (any->nl) */
             1,     Any,    NUL,    No,       Eat,    Prev, Any,
             1,     Any,    SPC,    No,       Eat,    Prev, Any,
             1,     Any,    Any,    No,       Bitch,  Prev, Any,
/* 0-so (start->red) */
             2,     Odd,    NL,     Yes,      Emit,   1,    Any,
             2,     Any,    SO,     No,       Eat,    2,    Yes,
             2,     Any,    SI,     No,       Unred,  0,    Yes,
             2,     Any,    DLE,    No,       Eat,    3,    Any,
             2,     Any,    DC1,    No,       Eat,    4,    Any,
             2,     Any,    DC2,    No,       Sub,    8,    Yes,
             2,     Any,    DC3,    No,       Eat,    6,    Any,
             2,     Any,    DC4,    No,       Super,  9,    Yes,
             2,     Any,    Any,    Yes,      Emit,   2,    Any,
/* dle (any->copy) */
             3,     Any,    Any,    No,       Copy,   Prev, Any,
/* dc1 (any->htab) */
             4,     Any,    Any,    No,       Spaces, Prev, Any,
/* 0-dc2 (start->sub) */
             5,     Odd,    NL,     Yes,      Emit,   1,    Any,
             5,     Any,    SO,     No,       Red,    10,   Yes,
             5,     Any,    SI,     No,       Eat,    5,    Yes,
             5,     Any,    DLE,    No,       Eat,    3,    Any,
             5,     Any,    DC1,    No,       Eat,    4,    Any,
             5,     Any,    DC2,    No,       Eat,    5,    Yes,
             5,     Any,    DC3,    No,       Eat,    6,    Any,
             5,     Any,    DC4,    No,       Unsub,  0,    Yes,
             5,     Any,    Any,    Yes,      Emit,   5,    Any,
/* dc3 (any->vtab) */
             6,     Any,    Any,    No,       Feeds,  Prev, Any,
/* 0-dc4 (start->super) */
             7,     Odd,    NL,     Yes,      Emit,   1,    Any,
             7,     Any,    SO,     No,       Red,    11,   Yes,
             7,     Any,    SI,     No,       Eat,    7,    Yes,
             7,     Any,    DLE,    No,       Eat,    3,    Any,
             7,     Any,    DC1,    No,       Eat,    4,    Any,
             7,     Any,    DC2,    No,       Unsup,  0,    Yes,
             7,     Any,    DC3,    No,       Eat,    6,    Any,
             7,     Any,    DC4,    No,       Eat,    7,    Yes,
             7,     Any,    Any,    Yes,      Emit,   7,    Any,
/* 2-dc2 (red->sub) */
             8,     Odd,    NL,     Yes,      Emit,   1,    Any,
             8,     Any,    SO,     No,       Eat,    8,    Yes,
             8,     Any,    SI,     No,       Eat,    8,    Yes,
             8,     Any,    DLE,    No,       Eat,    3,    Any,
             8,     Any,    DC1,    No,       Eat,    4,    Any,
             8,     Any,    DC2,    No,       Eat,    8,    Any,
             8,     Any,    DC3,    No,       Eat,    6,    Any,
             8,     Any,    DC4,    No,       Unsub,  2,    Any,
             8,     Any,    Any,    Yes,      Emit,   8,    Any,
/* 2-dc4 (red->super) */
             9,     Odd,    NL,     Yes,      Emit,   1,    Any,
             9,     Any,    SO,     No,       Eat,    9,    Yes,
             9,     Any,    SI,     No,       Eat,    9,    Yes,
             9,     Any,    DLE,    No,       Eat,    3,    Any,
             9,     Any,    DC1,    No,       Eat,    4,    Any,
             9,     Any,    DC2,    No,       Unsup,  2,    Yes,
             9,     Any,    DC3,    No,       Eat,    6,    Any,
             9,     Any,    DC4,    No,       Eat,    9,    Any,
             9,     Any,    Any,    Yes,      Emit,   9,    Any,
/* 5-so (sub->red) */
             10,    Odd,    NL,     Yes,      Emit,   1,    Any,
             10,    Any,    SO,     No,       Eat,    10,   Yes,
             10,    Any,    SI,     No,       Unred,  5,    Yes,
             10,    Any,    DLE,    No,       Eat,    3,    Any,
             10,    Any,    DC1,    No,       Eat,    4,    Any,
             10,    Any,    DC2,    No,       Eat,    10,   Yes,
             10,    Any,    DC3,    No,       Eat,    6,    Any,
             10,    Any,    DC4,    No,       Eat,    10,   Yes,
             10,    Any,    Any,    Yes,      Emit,   10,   Any,
/* 7-so (super->red) */
             11,    Odd,    NL,     Yes,      Emit,   1,    Any,
             11,    Any,    SO,     No,       Eat,    11,   Yes,
             11,    Any,    SI,     No,       Unred,  7,    Yes,
             11,    Any,    DLE,    No,       Eat,    3,    Any,
             11,    Any,    DC1,    No,       Eat,    4,    Any,
             11,    Any,    DC2,    No,       Eat,    11,   Yes,
             11,    Any,    DC3,    No,       Eat,    6,    Any,
             11,    Any,    DC4,    No,       Eat,    11,   Yes,
             11,    Any,    Any,    Yes,      Emit,   11,   Any,
/* End marker */
             -2,    -2,     -2,     -2,       -2,     -2,   -2
};
