/* Taken from utf8.c by Jeff Bezanson
   placed in the public domain Fall 2005 */

#include <sys/types.h>

int toutf8(char *dest, int sz, u_int32_t *src, int srcsz)
{
    u_int32_t ch;
    int i = 0;
    char *dest_end = dest + sz;

    while (srcsz<0 ? src[i]!=0 : i < srcsz) {
	ch = src[i];
	if (ch < 0x80) {
	    if (dest >= dest_end)
		return i;
	    *dest++ = (char)ch;
	}
	else if (ch < 0x800) {
	    if (dest >= dest_end-1)
		return i;
	    *dest++ = (ch>>6) | 0xC0;
	    *dest++ = (ch & 0x3F) | 0x80;
	}
	else if (ch < 0x10000) {
	    if (dest >= dest_end-2)
		return i;
	    *dest++ = (ch>>12) | 0xE0;
	    *dest++ = ((ch>>6) & 0x3F) | 0x80;
	    *dest++ = (ch & 0x3F) | 0x80;
	}
	else if (ch < 0x110000) {
	    if (dest >= dest_end-3)
		return i;
	    *dest++ = (ch>>18) | 0xF0;
	    *dest++ = ((ch>>12) & 0x3F) | 0x80;
	    *dest++ = ((ch>>6) & 0x3F) | 0x80;
	    *dest++ = (ch & 0x3F) | 0x80;
	}
	i++;
    }
    if (dest < dest_end)
	*dest = '\0';
    return i;
}
